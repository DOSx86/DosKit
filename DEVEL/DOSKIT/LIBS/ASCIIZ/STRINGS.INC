; BSD 3-Clause License
; Copyright (c) 2022-2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; *****************************************************************************
%ifndef ASCIIZ_STRINGS_INC_INCLUDED ; check that this file has not been included
%define ASCIIZ_STRINGS_INC_INCLUDED
; *****************************************************************************
;%include 'defaults.inc'
;%include 'nasmplus.inc'
;%include 'asciiz/chars.inc'
; *****************************************************************************
; Find the end of an AsciiZ and return the pointer to the following byte.
; Good for scanning compact lists of AsciiZ strings, like the Environment table.
; It scans according to the direction flag, you probably want it cleared at some
; point before the call. Also, flags are not preserved.
; entry: %1 = Pointer (if segment is not provided assumes ES, prefers ES:DI)
; exit:	 %1 = Updated Pointer
;	 %2 = optional, if register provided will contain bytes traveled.
;	      In other words, ENTRY %1 + EXIT %2 = EXIT %1
; *****************************************************************************
%imacro AsciiZNext 1-2
	%ifnidni %2, cx
		push 	cx
	%endif
	push    ax

	REG_TYPE %1

	%ifdef REG_PAIR
		; %warning REG_PAIR_HIGH:REG_PAIR_LOW
		%ifnidni REG_PAIR_HIGH, es
			push 	es
			mov  	es, REG_PAIR_HIGH
		%endif
		%ifnidni REG_PAIR_LOW, di
			push 	di
			mov  	di, REG_PAIR_LOW
		%endif
	%else
		%ifnidni %1, di
			push 	di
			mov  	di, %1
		%endif
	%endif

	xor	cx, cx
	dec	cx
	xor	ax, ax
	repne	scasb

	REG_TYPE %1
	%ifdef REG_PAIR
		%ifnidni REG_PAIR_LOW, di
			mov  	REG_PAIR_LOW,di
			pop  	di
		%endif
		%ifnidni REG_PAIR_HIGH, es
			pop  	es
		%endif
	%else
		%ifnidni %1, di
			mov  	%1,di
			pop	di
		%endif
	%endif

	pop	ax
	%if %0 = 2
		; Original DI + CX -> NEW DI
		not cx
	%endif

	%ifidni %2, cx
	%elif %0 = 2
		mov	%2, cx
		pop	cx
	%else
		pop	cx
	%endif

%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZCompare_INTERNAL 0

AsciiZCompare_PROC:
	push		cx
	xor		cx, cx
	jmp		%%Main
.Exact:
	push		cx
	xor		cx, cx
	inc		cx
	jmp		%%Main

.Preset:
	push		cx
	xor		ch,ch

%%Main:
	push		ax
	; cx 0 Caseless, cx 1 case specific
	xor		bx,bx
%%Next:
	mov		al, [si+bx]	; DS:SI+BX
	mov		ah, [es:di+bx]
	call		%%TestEnd
	call		%%TestEnd
	cmp		ch, 1
	je		%%Fail
	ja		%%Pass
	test		cl, cl
	jnz		%%ExactOnly
	UpperCase	al
	UpperCase	ah
%%ExactOnly:
	cmp		al, ah
	jne		%%Fail
	inc		bx
	jmp		%%Next

%%TestEnd:
	xchg		al, ah
	test		al, al
	jz		%%IsEnd
	cmp		al, dl
	je		%%IsEnd
	cmp		al, dh
	je		%%IsEnd
	ret
%%IsEnd:
	inc		ch
	ret
%%Fail:
	cmp		al, ah
	stc
	jmp		%%Done
%%Pass:
	cmp		al, al
	clc
%%Done:
	pop		ax, cx
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZCompare_SUBMAC 2-4
	REQUIRE AsciiZCompare
	%if %0 > 2
		pushndr ds:si, %1, es:di, %2, dx, %3
		setndr  ds:si, %1, es:di, %2
		setndr  dx, %3
	%else
		push	dx
		pushndr ds:si, %1, es:di, %2
		setndr  ds:si, %1, es:di, %2
		xor	dx, dx
	%endif
	%ifidni %4, Exact
		call 	AsciiZCompare_PROC.Exact
	%else
		call 	AsciiZCompare_PROC
	%endif
	%if %0 > 2
		popndr ds:si, %1, es:di, %2, dx, %3
	%else
		popndr ds:si, %1, es:di, %2
		pop	dx
	%endif
%endmacro
; *****************************************************************************
; Compare two AsciiZ strings for equality.
; entry: %1 = Label (in DS) or Pointer (prefers ds:si) with AsciiZ
;	 %2 = Pointer (prefers es:di) to second AsciiZ
;	 %3 = optional word with two additional string terminators. For example,
;	      0x003d is Null + =, which is good for environment strings. While
;	      0x203f is Space + /, which could be helpful parsing command line
;	      strings.
;	 %4 = optional, normally, upper and lower case letters are assumed to
; 	      be a match. If case specific only matches are needed, provide
;	      the option EXACT to enable it.
; exit:	 CY set if not a match
;	 CY clear if was a match
;	 BX is returned as the offset where comparison stopped.
; 	 Also modifies other flags to easily use JB, JA, etc.
; *****************************************************************************
%imacro AsciiZCompare 2-4
	REG_TYPE %1
	%ifidni REG_CLASS, TOKEN
		REG_TEMP 	bx, %{2:-1}
		; needed to save temp, cause SetNDR will overwrite it
		%xdefine 	AsciiZ_Temp_Reg TEMP_REGISTER
		push 		AsciiZ_Temp_Reg
		mov		AsciiZ_Temp_Reg, %1
		AsciiZCompare_SUBMAC ds:AsciiZ_Temp_Reg, %{2:-1}
		pop  		AsciiZ_Temp_Reg
	%else
		AsciiZCompare_SUBMAC %{1:-1}
	%endif
%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZLookup_INTERNAL 0

AsciiZLookup_PROC:
	push		cx
	xor		cx, cx
	jmp		%%Main
.Exact:
	push		cx
	xor		cx, cx
	inc		cx

%%Main:
	push		di,ax
%%Next:
	xor		ch,ch
	call		AsciiZCompare_PROC.Preset
	jnc		%%Pass
	mov		al, [es:di]
	test		al,al
	jz		%%Fail
	AsciiZNext	es:di
	jmp		%%Next
%%Fail:
	stc
	jmp		%%Done
%%Pass:
	test		bx,bx
	jz		%%Fail
	add		bx,di
	inc		bx
	clc
%%Done:
	pop		ax, di, cx
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZLookup_SUBMAC 2-4
	REQUIRE AsciiZCompare
	REQUIRE AsciiZLookup
	%if %0 > 2
		pushndr ds:si, %1, es:di, %2, dx, %3
		setndr  ds:si, %1, es:di, %2
		setndr  dx, %3
	%else
		push	dx
		pushndr ds:si, %1, es:di, %2
		setndr  ds:si, %1, es:di, %2
		xor	dx, dx
	%endif
	%ifidni %4, Exact
		call 	AsciiZLookup_PROC.Exact
	%else
		call 	AsciiZLookup_PROC
	%endif
	%if %0 > 2
		popndr ds:si, %1, es:di, %2, dx, %3
	%else
		popndr ds:si, %1, es:di, %2
		push	dx
	%endif
%endmacro
; *****************************************************************************
; Search a null terminated list of AsciiZ strings for one starting with a
; specific string.
; entry: %1 = Label (in DS) or Pointer (prefers ds:si) with search AsciiZ
;	 %2 = Pointer (prefers es:di) to null terminated list of AsciiZ strings
;	 %3 = optional word with two additional string terminators. For example,
;	      0x003d is Null + =, which is good for the environment table.
;	 %4 = optional, normally, upper and lower case letters are assumed to
; 	      be a match. If case specific only matches are needed, provide
;	      the option EXACT to enable it.
; exit:	 CY set if not a match
;	 CY clear if was a match
;	 BX points to the text following the terminator character.
; *****************************************************************************
%imacro AsciiZLookup 2-4
	REG_TYPE %1
	%ifidni REG_CLASS, TOKEN
		REG_TEMP 	bx, %{2:-1}
		needed to save temp, cause SetNDR will overwrite it
		%xdefine 	AsciiZ_Temp_Reg TEMP_REGISTER
		push 		AsciiZ_Temp_Reg
		mov		AsciiZ_Temp_Reg, %1
		AsciiZLookup_SUBMAC ds:AsciiZ_Temp_Reg, %{2:-1}
		stc
		pop  		AsciiZ_Temp_Reg
	%else
		AsciiZLookup_SUBMAC %{1:-1}
	%endif
%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZField_INTERNAL 0

AsciiZNoField_PROC:
	push	dx
	mov	dl, ','
	call 	AsciiZField_PROC
	pop	dx
	ret

AsciiZField_PROC:
	push		bx, ax, cx, si, di
	cld
%%IndexLoop:
	test		cx, cx
	jz		%%Found
%%ScanLoop:
	lodsb
	test		al, al
	jz		%%NotFound
	cmp		al, dl
	jne		%%ScanLoop
	dec		cx
	jmp		%%IndexLoop
%%Found:
	test		bx, bx
	jz		%%FoundDone
	dec		bx
	lodsb
	test		al, al
	jz		%%FoundDone
	cmp		al, dl
	je		%%FoundDone
	stosb
	jmp		%%Found
%%FoundDone:
	xor		al, al
	clc
	jmp		%%Done

%%NotFound:
	stc
%%Done:
	stosb
	pop		di, si, cx, ax, bx
	ret

%endmacro
; *****************************************************************************
; SRC, DEST, INDEX, MAX_CHARS, [SEPARATOR]
%imacro AsciiZField 4-5
	REQUIRE AsciiZField
	%if %0 = 5
		pushndr ds:si, %1, es:di, %2, cx, %3, bx, %4, dl, %5
		setndr  ds:si, %1, es:di, %2
		setndr  cx, %3, bx, %4, dl, %5
		call	AsciiZField_PROC
		popndr  ds:si, %1, es:di, %2, cx, %3, bx, %4, dl, %5
	%else
		pushndr ds:si, %1, es:di, %2, cx, %3, bx, %4
		setndr  ds:si, %1, es:di, %2
		setndr  cx, %3, bx, %4
		call	AsciiZNoField_PROC
		popndr ds:si, %1, es:di, %2, cx, %3, bx, %4
	%endif
%endmacro
; *****************************************************************************
%macro AsciiZCopyMax_INTERNAL 0
AsciiZCopyMax_PROC:
		push	ax, cx, si, di
		cld
	%%Loop:
		test	cx, cx
		jz	%%Done
		lodsb
		test	al, al
		jz	%%Done
		stosb
		dec	cx
		jmp	%%Loop
	%%Done:
		xor	al, al
		stosb
		pop	di, si, cx, ax
		ret
%endmacro

%macro AsciiZCopy_INTERNAL 0
AsciiZCopy_PROC:
		push	ax, si, di
		cld
	%%Loop:
		lodsb
		stosb
		test	al, al
		jnz	%%Loop
		pop	di, si, ax
		ret
%endmacro

%imacro AsciiZCopy 2-3
	%if %0 = 3
		REQUIRE AsciiZCopyMax
		pushndr ds:si, %1, es:di, %2, cx, %3
		setndr  ds:si, %1, es:di, %2
		setndr  cx, %3
		call	AsciiZCopyMax_PROC
		popndr ds:si, %1, es:di, %2, cx, %3
	%else
		REQUIRE AsciiZCopy
		pushndr ds:si, %1, es:di, %2
		setndr  ds:si, %1, es:di, %2
		call	AsciiZCopy_PROC
		popndr ds:si, %1, es:di, %2
	%endif
%endmacro
; *****************************************************************************
%macro AsciiZCopyIncMax_INTERNAL 0
AsciiZCopyIncMax_PROC:
		push	ax, si
		cld
	%%Loop:
		test	cx, cx
		jz	%%Done
		lodsb
		test	al, al
		jz	%%Done
		stosb
		dec	cx
		jmp	%%Loop
	%%Done:
		xor	al, al
		stosb
		dec	di
		pop	si, ax
		ret
%endmacro

%macro AsciiZCopyInc_INTERNAL 0
AsciiZCopyInc_PROC:
		push	ax, si
		cld
	%%Loop:
		lodsb
		stosb
		test	al, al
		jnz	%%Loop
		dec	di
		pop	si, ax
		ret
%endmacro

%imacro AsciiZCopyInc 2-3
	%if %0 = 3
		REQUIRE AsciiZCopyIncMax
		pushndr ds:si, %1, es:di, %2, cx, %3
		setndr  ds:si, %1, es:di, %2
		setndr  cx, %3
		call	AsciiZCopyIncMax_PROC
		setndr  %2, es:di
		setndr  %3, cx
		popndr ds:si, %1, es:di, %2, cx, %3
	%else
		REQUIRE AsciiZCopyInc
		pushndr ds:si, %1, es:di, %2
		setndr  ds:si, %1, es:di, %2
		call	AsciiZCopyInc_PROC
		setndr  %2, es:di
		popndr ds:si, %1, es:di, %2
	%endif
%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZReplaceChar_INTERNAL 0

AsciiZReplaceChar_PROC:
	push		di, bx
%%Replace:
	mov		bl, [es:di]
	test		bl, bl
	jz		%%Done
	cmp		bl, al
	jne		%%Next
	mov		[es:di], ah
%%Next:
	inc		di
	jmp		%%Replace
%%Done:
	pop		bx, di
	ret

%endmacro
; *****************************************************************************
%imacro AsciiZReplaceChar 2-3
REQUIRE AsciiZReplaceChar
	%if %0 = 3
		PushNDR		es:di, %1, al, %2, ah, %3
		SetNDR		es:di, %1, al, %2, ah, %3
		call		AsciiZReplaceChar_PROC
		PopNDR		es:di, %1, al, %2, ah, %3
	%else
		PushNDR		es:di, %1, ax, %2
		SetNDR		es:di, %1, ax, %2
		call		AsciiZReplaceChar_PROC
		PopNDR		es:di, %1, ax, %2
	%endif
%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZLength_INTERNAL 0
AsciiZLength_PROC:
	push	bx
	xor	bx, bx
%%Counting:
	test	[es:di+bx], byte 0xff
	jz	%%Done
	inc	bx
	jnz	%%Counting
%%Done:
	mov	cx, bx
	pop	bx
	ret
%endmacro
; *****************************************************************************
%imacro AsciiZLength 2
	REQUIRE AsciiZLength
	PushNDR es:di, %1, cx, %2
	SetNDR  es:di, %1
	call	AsciiZLength_PROC
	SetNDR  %2, cx
	PopNDR  es:di, %1, cx, %2
%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZTrimWhitespace_INTERNAL 0

AsciiZTrimWhitespace_PROC:
	push		ds, di, ax
	mov		ds, es
	mov		di, si
	cld
	push		si
	xor		ah, ah
%%CopyLeft:
	lodsb
	test		ah, ah
	jnz		%%CopyChar
	cmp		al, 0x20
	je		%%NoCopy
	cmp		al, 0x9
	je		%%NoCopy
	inc		ah
%%CopyChar:
	stosb
%%NoCopy:
	test		al, al
	jnz		%%CopyLeft
	pop		si
%%PruneRight:
	dec		di
	cmp		si, di		; at string start, we are done
	je		%%Done
	mov		al, [di]
	cmp		al, 0x20
	je		%%PruneChar
	cmp		al, 0x09
	je		%%PruneChar
	test		al, al		; first pass we are on terminator
	jz		%%PruneRight
	jmp		%%Done
%%PruneChar:
	mov		[di], byte 0x00
	jmp		%%PruneRight
%%Done:
	pop		ax, di, ds
	ret

%endmacro
; *****************************************************************************
%imacro AsciiZTrimWhitespace 1
REQUIRE AsciiZTrimWhitespace

	PushNDR		es:di, %1
	SetNDR		es:di, %1
	call		AsciiZTrimWhitespace_PROC
	PopNDR		es:di, %1

%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZUpperCase_INTERNAL 0

AsciiZUpperCase_PROC:
	push		es, di, ax
	cld
%%Loop:
	mov		al, [es:di]
	test		al, al
	jz		%%Done
	UpperCase	al
	es stosb
	jmp		%%Loop
%%Done:

	pop		ax, di, es
	ret

%endmacro
; *****************************************************************************
%imacro AsciiZUpperCase 1
REQUIRE AsciiZUpperCase

	PushNDR		es:di, %1
	SetNDR		es:di, %1
	call		AsciiZUpperCase_PROC
	PopNDR		es:di, %1

%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZLowerCase_INTERNAL 0

AsciiZLowerCase_PROC:
	push		es, di, ax
	cld
%%Loop:
	mov		al, [es:di]
	test		al, al
	jz		%%Done
	LowerCase	al
	es stosb
	jmp		%%Loop
%%Done:

	pop		ax, di, es
	ret

%endmacro
; *****************************************************************************
%imacro AsciiZLowerCase 1
REQUIRE AsciiZLowerCase

	PushNDR		es:di, %1
	SetNDR		es:di, %1
	call		AsciiZLowerCase_PROC
	PopNDR		es:di, %1

%endmacro
; -----------------------------------------------------------------------------
%imacro AsciiZFromHex_INTERNAL 0
AsciiZFromHex_PROC:
	push	si, di, ax
	mov	si, di
%%aLittleLoopy:
	mov	ax, [es:si]
	test	al, al
	jz	%%Done
	cmp	al, 0x20  ; space
	je	%%SkipOne
	cmp	al, 0x3b  ; Semicolon
	je	%%SkipOne
	cmp	al, 0x2c  ; comma
	jne	%%NoComma
%%SkipOne:
	inc	si
	jmp	%%aLittleLoopy
%%NoComma:
	cmp	ax, 0x7830
	jne	%%NoPrefix
	add	si, 2
	jmp	%%aLittleLoopy
%%NoPrefix:
	call	ByteValueOfHex_PROC
	jc	%%Fail
	mov	[es:di], al
	inc	di
	add	si, 2
	jmp	%%aLittleLoopy
%%Fail:
	stc
	jmp	%%Done
%%Success:
	clc
%%Done:
	mov	al, 0
	mov	[es:di], al
	pop	ax, di, si
	ret
%endmacro
; *****************************************************************************
%imacro AsciiZFromHex 1
REQUIRE AsciiZFromHex
REQUIRE ByteValueOfHex
	PushNDR es:di, %1
	SetNDR es:di, %1
	call	AsciiZFromHex_PROC
	PopNDR es:di, %1
%endmacro
; -----------------------------------------------------------------------------
%imacro AsciiZMov_INTERNAL 0
AsciiZMov_PROC:
	pushf
	push	ax, si, di
	cld
%%Loop:
	lodsb
	es stosb
	test	al, al
	jnz	%%Loop
	pop	di, si, ax
	popf
	ret
%endmacro
; *****************************************************************************
%imacro AsciiZMov 2
REQUIRE AsciiZMov
	PushNDR es:di, %1
	PushNDR ds:si, %2
	SetNDR es:di, %1
	SetNDR ds:si, %2
	call	AsciiZMov_PROC
	PopNDR ds:si, %2
	PopNDR es:di, %1
%endmacro
; -----------------------------------------------------------------------------
%imacro AsciiZIncludeTailDelim_INTERNAL 0
AsciiZIncludeTailDelim_PROC:
	push		bx, cx
	AsciiZLength	es:di, cx
	mov		bx, cx
	test		cx, cx
	jz		.NoTest
	cmp		[es:di+bx-1], al
	je		%%Done
.NoTest:
	mov		[es:di+bx], al
	inc		bx
	mov		[es:di+bx], byte 0
%%Done:
	pop		cx, bx
	ret
%endmacro
; *****************************************************************************
%imacro AsciiZIncludeTailDelim 2
REQUIRE AsciiZIncludeTailDelim
	%ifnidni %2, al
		push ax
	%endif
	PushNDR es:di, %1
	SetNDR  es:di, %1
	%ifnidni %2, al
		mov	al, %2
	%endif
	call	AsciiZIncludeTailDelim_PROC
	PopNDR es:di, %1
	%ifnidni %2, al
		pop ax
	%endif
%endmacro
; -----------------------------------------------------------------------------
%imacro AsciiZExcludeTailDelim_INTERNAL 0
AsciiZExcludeTailDelim_PROC:
	push		bx, cx
	AsciiZLength	es:di, cx
	test		cx, cx
	jz		%%Done
	mov		bx, cx
	cmp		[es:di+bx], al
	jne		%%Done
	mov		[es:di+bx], byte 0
%%Done:
	pop		cx, bx
	ret
%endmacro
; *****************************************************************************
%imacro AsciiZExcludeTailDelim 2
REQUIRE AsciiZExcludeTailDelim
	PushNDR es:di, %1, al, %2
	SetNDR  es:di, %1, al, %2
	call	AsciiZIExcludeTailDelim_PROC
	PopNDR es:di, %1, al, %2
%endmacro
; *****************************************************************************
%imacro AsciiZIsLastChar 2
	REQUIRE AsciiZLength
	PushNDR es:di, %1, al, %2
	SetNDR  es:di, %1
	SetNDR	al, %2
	push	bx
	AsciiZLength es:di, bx
	test	bx,bx
	jz	%%Nope
	cmp	[es:di+bx-1], al
	jne	%%Nope
	clc
	jmp	%%Done
%%Nope:
	stc
%%Done:
	pop	bx

	PopNDR es:di, %1, al, %2
%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZAppend_INTERNAL 0

AsciiZAppend_PROC:
	push	ax, si, di, cx
	cmp	cx, 1
	jbe	%%Failed
	cld
%%FindEnd:
	mov	al, [es:di]
	test	al, al
	jz	%%Append
	inc	di
	loop	%%FindEnd
%%Failed:
	xor	al, al
	stosb
	stc
	jmp	%%Done
%%Append:
	lodsb
	stosb
	test	al, al
	jz	%%Success
	loop	%%Append
	jmp	%%Failed
%%Success:
	clc
%%Done:
	pop	cx, di, si, ax
	ret

%endmacro
; *****************************************************************************
%imacro AsciiZAppend 3
	REQUIRE AsciiZAppend
	%if %0 = 3
		PushNDR es:di, %1, ds:si, %2, cx, %3
		SetNDR es:di, %1, ds:si, %2, cx, %3
		call	AsciiZAppend_PROC
		PopNDR es:di, %1, ds:si, %2, cx, %3
;	%else
;		PushNDR es:di, %1, ds:si, %2
;		SetNDR es:di, %1, ds:si, %2
;		call	AsciiZAppendNoLimit_PROC
;		PopNDR es:di, %1, ds:si, %2

	%endif
%endmacro
; -----------------------------------------------------------------------------
%macro AsciiZAppendChar_INTERNAL 0

AsciiZAppendChar_PROC:
	push		di, cx
	AsciiZLength	es:di, cx
	add		di, cx
	mov		[es:di], al
	mov		[es:di + 1], byte 0
	pop		cx, di
	ret

%endmacro
; *****************************************************************************
%imacro AsciiZAppendChar 2
	REQUIRE AsciiZAppendChar
	REQUIRE AsciiZLength
	PushNDR es:di, %1, al, %2
	SetNDR es:di, %1, al, %2
	call	AsciiZAppendChar_PROC
	PopNDR es:di, %1, al, %2
%endmacro
; *****************************************************************************
%imacro AsciiZPosChar 2-3
	push	es, di
	SetNDR es:di, %1
	%if %0 = 3
		PushNDR ax, %3
	%endif
		push	cx
		xor     cx, cx
	%%Find:
		mov	al, [es:di]
		cmp	al, %2
		je	%%Found
		inc	di
		inc	cx
		test	al, al
		jnz	%%Find
		stc
		jmp	%%Done
	%%Found:
		mov	ax, cx
		clc
	%%Done:
		pop	cx
	%if %0 = 3
		SetNDR %3, ax
		PopNDR ax, %3
	%endif
	pop	di, es
%endmacro
; *****************************************************************************
%else				; if file has already been included
; *****************************************************************************
	PUSH_SECTION .proc
		PROVIDE AsciiZLength
		PROVIDE AsciiZCompare
		PROVIDE AsciiZLookup
		PROVIDE AsciiZField
		PROVIDE AsciiZCopyMax
		PROVIDE AsciiZCopy
		PROVIDE AsciiZCopyIncMax
		PROVIDE AsciiZCopyInc
		PROVIDE AsciiZReplaceChar
		PROVIDE AsciiZTrimWhitespace
		PROVIDE AsciiZUpperCase
		PROVIDE AsciiZLowerCase
		PROVIDE AsciiZFromHex
		PROVIDE AsciiZMov
		PROVIDE AsciiZIncludeTailDelim
		PROVIDE AsciiZExcludeTailDelim
		PROVIDE AsciiZAppend
		PROVIDE AsciiZAppendChar
	POP_SECTION
%endif
