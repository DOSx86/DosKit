; BSD 3-Clause License
; Copyright (c) 2022-2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; *****************************************************************************
%ifndef CORECRT_CLEAR_INC_DEFINED
%define CORECRT_CLEAR_INC_DEFINED
; *****************************************************************************
%macro ClrScr_INTERNAL 0

ClrScr_PROC:
	push			ax, bx, cx, dx
	mov			cx, [CRT.WindMin]
	mov			dx, [CRT.WindMax]
Clear_Region_PROC:
	mov			bh, [CRT.Attr]

	MousePreWrite		cx, dx

	push			cx, dx

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		test 	[CRT.Options], byte vcbDirect
		jz	ClrScr_PROC_BIOS
		jmp	ClrScr_PROC_DIRECT
	%endif

%ifdef CRT_BIOS_VIDEO
ClrScr_PROC_BIOS:
	mov			ax, 0x0600	; Scroll Up 0 lines
	%ifdef DOS_BUG_FIXES
		; See -- https://fd.lod.bz/rbil/interrup/video/1006.html#130
		push		ds, bp
	%endif
	int			0x10
	%ifdef DOS_BUG_FIXES
		pop		bp, ds
	%endif
%endif

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		jmp			%%Done
	%endif

%ifdef CRT_DIRECT_VIDEO

ClrScr_PROC_DIRECT:
	push 			es, di, cx, dx
	mov			bl, 0x20
	CalcVideoPtr		cx  ; es:di=video ptr, ax is now bytes per line
	xchg			ax, bx	    ; ax=char/attr, bx=bytes/line

	sub			dx, cx
	add			dx, 0x0101 ; dl=column count, dh=row count

	cld
%%ClearRow:
	push			di, dx
%%ClearLine:
	stosw
	dec			dl
	jnz			%%ClearLine
	pop			dx, di
	add			di, bx
	dec			dh
	jnz			%%ClearRow

	pop			dx, cx, di, es
%endif

%%Done:
	MousePostWrite
	xor			ax, ax
	call			GotoXY_PROC 	; ax = position
	pop			dx, cx
	%ifdef CRT_SCROLL_HOOK
		push		cx, dx, ax
		mov	      	ax, 0xff00
		cmp		cx, word 0000
		jne		%%Partial
		cmp		dx, [CRT.Max]
		jne		%%Partial
		mov		al, 1
	%%Partial:
		CRTScrollHook 	ah
		pop		ax, dx, cx
	%endif
	pop			dx, cx, bx, ax
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro ClrEOL_INTERNAL 0

ClrEOL_PROC:
	push			dx
	WhereXY			dx
	call			.PerformClear
	GotoXY			dx
	pop			dx
	ret
.PerformClear:
	push			ax, bx, cx, dx
	mov			cx, dx
	mov			dl, [CRT.WindMax.X]
	jmp			Clear_Region_PROC

%endmacro
; -----------------------------------------------------------------------------
%macro ClrArea_INTERNAL 0

ClrArea_PROC:
	push			ax
	WhereXY			ax
	call			.PerformClear
	GotoXY			ax
	pop			ax
	ret
.PerformClear:
	push			ax, bx, cx, dx
	jmp			Clear_Region_PROC

%endmacro
; *****************************************************************************
; Clear the entire Window.
; *****************************************************************************
%imacro ClrScr 0

	REQUIRE GotoXY
	REQUIRE ClrScr

	call 			ClrScr_PROC

%endmacro
; *****************************************************************************
; Clear the remaining portion of the current line, cursor does not move
; MACRO: ClrEOL 0
; *****************************************************************************
%imacro ClrEOL 0

	REQUIRE WhereXY
	REQUIRE ClrScr
	REQUIRE ClrEOL

	call 			ClrEOL_PROC

%endmacro
; *****************************************************************************
; Clear an area of the screen, cursor does not move
; MACRO: ClrArea 2
; INPUT: %1 = MinXY, %2 = MaxXY (prefers cx, dx)
; *****************************************************************************
%imacro ClrArea 2

	REQUIRE ClrScr
	REQUIRE ClrArea

	PushNDR			cx, %1, dx, %2
	SetNDR			cx, %1, dx, %2
	call 			ClrArea_PROC
	PopNDR			cx, %1, dx, %2

%endmacro
; *****************************************************************************
%else
; *****************************************************************************
	PUSH_SECTION .proc
		PROVIDE ClrScr
		PROVIDE ClrEOL
		PROVIDE ClrArea
	POP_SECTION
%endif
