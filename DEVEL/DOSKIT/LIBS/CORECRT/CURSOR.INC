; BSD 3-Clause License
; Copyright (c) 2022-2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; *****************************************************************************
%ifndef CORECRT_CURSOR_INC_DEFINED
%define CORECRT_CURSOR_INC_DEFINED
; -----------------------------------------------------------------------------
%macro GetCursor_INTERNAL 0

GetCursor_PROC:
	push			ax, bx, dx
	mov			bh, [CRT.Page]
	mov			ah, 0x03		; Get cursor shape/pos
	int			0x10
	pop 			dx, bx, ax
	ret

%endmacro
; *****************************************************************************
; Return the current cursor shape
; MACRO:  GetCursor 1
; OUTPUT: %1 = Cursor Shape (prefers cx)
; *****************************************************************************

%imacro GetCursor 1

	REQUIRE 		GetCursor

	pushndr			cx, %1
	call 			GetCursor_PROC
	setndr			%1, cx
	popndr			cx, %1

%endmacro
; -----------------------------------------------------------------------------
; Set the current cursor shape
; MACRO:  SetCursor 1
; INPUT:  %1 = Cursor Shape (prefers cx) (accepts: original, small, half, full)
;	  Also, it uses HIDE, SHOW with counters. 2 hides will need 2 shows.
;	  Like the cursor itself, the counters are reset on mode changes.
; REGS:   ax, cx
; -----------------------------------------------------------------------------
%macro SetCursor_INTERNAL 0

SetCursor_PROC:			; never enter this door, well maybe sometimes
	push			ax,cx

SetCursor_PROC1:		; maybe have cursor go hide
	mov			[CRT.Cursor.Shape], cx
	cmp			[CRT.Cursor.State], word 0x0000
	jl			SetCursor_PROC3	; Signed Integer < 0 not visible
SetCursor_PROC2:		; Change visible cursor shape and show cursor
	mov			ah, 0x01
	int			0x10
SetCursor_PROC3:		; don't do nut'in
	pop			cx,ax
	ret

%endmacro

%macro SetCursorShow_INTERNAL 0

SetCursorShow_PROC:
	push			ax, cx
	inc			word [CRT.Cursor.State]
	cmp			[CRT.Cursor.State], word 0x0000 ; how about jz
	jne			SetCursor_PROC3	; Signed Int <> 0 leave it alone
	mov			cx, [CRT.Cursor.Shape]
	jmp			SetCursor_PROC2 ; make it so
%endmacro

%macro SetCursorHide_INTERNAL 0

SetCursorHide_PROC:
	push			ax, cx
	dec			word [CRT.Cursor.State]
	cmp			[CRT.Cursor.State], word -1
	jne			SetCursor_PROC3 ; if it did not become invisible
	mov			cx, 0x2000
	jmp			SetCursor_PROC2 ; play hide and seek

%endmacro

%macro SetCursorSmall_INTERNAL 0

SetCursorSmall_PROC:
	push			ax, cx
	push			ds
	mov			ax, 0x0040
	mov			ds, ax
	mov			cx, [0x0085]
	pop			ds
	mov			ch, cl
	sub			ch, 2
	jmp			SetCursor_PROC1 ; change if visible

%endmacro

%macro SetCursorHalf_INTERNAL 0

SECTION_PROC

SetCursorHalf_PROC:
	push			ax, cx
	push			ds
	mov			ax, 0x0040
	mov			ds, ax
	mov			al, [0x0085]
	pop			ds
	mov			ah, al
	shr			ah, 1
	mov			cx, ax
	jmp			SetCursor_PROC1

%endmacro

%macro SetCursorFull_INTERNAL 0

SetCursorFull_PROC:
	push			ax, cx
	push			ds
	mov			ax, 0x0040
	mov			ds, ax
	mov			cx, [0x0085]
	pop			ds
	xor			ch, ch
	jmp			SetCursor_PROC1

%endmacro

%macro SetCursorOriginal_INTERNAL 0

SetCursorOriginal_PROC:
	push			ax, cx
	mov			cx, [CRT.Initial.Cursor.Shape]
	jmp			SetCursor_PROC1

%endmacro

%macro SetCursorInvisible_INTERNAL 0

SetCursorInvisible_PROC:
	push			ax, cx
	mov			cx, 0x2000
	jmp			SetCursor_PROC1

%endmacro

%imacro SetCursor 1

	REQUIRE 		SetCursor

	%ifnidni %1, cx
		%ifidni %1, original
			REQUIRE 	SetCursorOriginal
			call 		SetCursorOriginal_PROC
		%elifidni %1, invisible ; you should avoid this one
			REQUIRE 	SetCursorInvisible
			call 		SetCursorInvisible_PROC
		%elifidni %1, small
			REQUIRE 	SetCursorSmall
			call		SetCursorSmall_PROC
		%elifidni %1, half
			REQUIRE 	SetCursorHalf
			call		SetCursorHalf_PROC
		%elifidni %1, full
			REQUIRE 	SetCursorFull
			call		SetCursorFull_PROC
		%elifidni %1, hide
			REQUIRE 	SetCursorHide
			call		SetCursorHide_PROC
		%elifidni %1, show
			REQUIRE 	SetCursorShow
			call		SetCursorShow_PROC
		%else
			pushndr		cx, %1
			setndr		cx, %1
			call 		SetCursor_PROC
			popndr		cx, %1
		%endif
	%else
		call 		SetCursor_PROC
	%endif

%endmacro

%idefine Cursor SetCursor
; *****************************************************************************
%else
	PUSH_SECTION .proc
		PROVIDE GetCursor
		PROVIDE SetCursor
		PROVIDE SetCursorOriginal
		PROVIDE SetCursorInvisible	; avoid this one
		PROVIDE SetCursorSmall
		PROVIDE SetCursorHalf
		PROVIDE SetCursorFull
		PROVIDE SetCursorShow
		PROVIDE SetCursorHide
	POP_SECTION
%endif
