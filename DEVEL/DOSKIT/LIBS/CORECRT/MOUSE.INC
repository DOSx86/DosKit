; BSD 3-Clause License
; Copyright (c) 2022-2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

; *****************************************************************************
%ifndef CORECRT_MOUSE_INC_DEFINED
%define CORECRT_MOUSE_INC_DEFINED
; *****************************************************************************
; -----------------------------------------------------------------------------
; Most of the stuff in this file is overhead required to support the mouse and
; does not need direct interaction by the end user (programmer/developer).
; Look for the end-user section bellow.
; -----------------------------------------------------------------------------
; if CRT_MOUSE is not defined, all mouse procs become NULL macros.
; *****************************************************************************
%define MOUSE_EVENT_SIZE 4 ; 1 word + 2 bytes per event buffer entry
; -----------------------------------------------------------------------------
%macro MouseSupportData_INTERNAL 0

MOUSE.BUTTON.COUNT:		resw 1
MOUSE.ORIGINAL.MASK:		resw 1
MOUSE.ORIGINAL.HANDLER:		resw 2

MOUSE.DIV.X:			resw 1
MOUSE.DIV.Y:			resw 1

MOUSE.MIN.X:			resw 1
MOUSE.MIN.Y:			resw 1

MOUSE.MAX.X:			resw 1
MOUSE.MAX.Y:			resw 1

MOUSE.CURSOR.STATE:		resw 1
MOUSE.CHARATTR:			resw 1
MOUSE.FLAG.REDRAW:		resb 1
MOUSE.FLAG.MOVE:		resb 1
MOUSE.BUTTON.STATE:		resw 1
MOUSE.BUTTON.LAST:		resw 1
MOUSE.FLAG.RELEASE:		resw 1

MOUSE.VALUE.X:			resw 1
MOUSE.VALUE.Y:			resw 1
MOUSE.POSITION.X:		resw 1
MOUSE.POSITION.Y:		resw 1

MOUSE.BUFFER.HEAD:		resw 1
MOUSE.BUFFER.TAIL:		resw 1
MOUSE.BUFFER.START:		resb 16 * MOUSE_EVENT_SIZE
MOUSE.BUFFER.END:

%endmacro

%macro MouseSupportProcs_INTERNAL 0

MouseInitialize_PROC:
	push			bx, ax
	%ifdef DOS_BUG_FIXES
		; See -- https://fd.lod.bz/rbil/interrup/io_mouse/330000.html
	%endif
	xor			ax, ax
	int			0x33
	test			ax, ax
	jz			%%SetMouseStatus
	mov			ax, 0x0001		; other than two btns
	cmp			bx, ax
	jbe			%%SetMouseStatus
	inc			ax
	cmp			bx, 0xffff		; sometimes two buttons
	je			%%SetMouseStatus
	mov			ax, bx
%%SetMouseStatus:
	mov			[MOUSE.BUTTON.COUNT], ax
	test			ax, ax
	jz			%%CheckDone			; No mouse found
	; If mouse is present swap out original mouse handler
	push			cx, dx, es
	mov			cx, 0xffff
	push			cs
	pop			es
	mov			dx, MouseHandler_INTERRUPT
	mov			ax, 0x0014
	int			0x33
	mov			[MOUSE.ORIGINAL.MASK], cx
	mov			[MOUSE.ORIGINAL.HANDLER], dx
	mov			[MOUSE.ORIGINAL.HANDLER + 2], es
	pop			es, dx, cx
%%CheckDone:
	pop			ax, bx
	ret

MouseFinalize_PROC:
	cmp     		[MOUSE.BUTTON.COUNT], word 0x0000
	je			%%DoneNoMouse
	; If mouse is present then hide it and restore original mouse
	push			cx, dx, es
	call			MouseHide_PROC
	mov			cx, [MOUSE.ORIGINAL.MASK]
	mov			dx, [MOUSE.ORIGINAL.HANDLER]
	mov			es, [MOUSE.ORIGINAL.HANDLER + 2]
	mov			ax, 0x0014
	int			0x33
	mov     		[MOUSE.BUTTON.COUNT], word 0x0000
	pop			es, dx, cx
%%DoneNoMouse:
	ret

MouseDataReset_PROC:
	push			ax, bx, cx, dx, ds
	mov			ds,cs
	mov			[MOUSE.CURSOR.STATE], word -1
	mov			ax, MOUSE.BUFFER.START
	mov			[MOUSE.BUFFER.HEAD], ax
	mov			[MOUSE.BUFFER.TAIL], ax
	xor			ax, ax
	mov			[MOUSE.FLAG.REDRAW], al
	mov			[MOUSE.FLAG.MOVE], al
	mov			[MOUSE.FLAG.RELEASE], ax
	mov			[MOUSE.BUTTON.STATE], ax
	mov			[MOUSE.MIN.X], ax
	mov			[MOUSE.MIN.Y], ax

	; Figure out X related values
	xor			ah, ah
	mov			bh, ah
	mov			al, [CRT.Max.X]   ; Max number of Columns - 1
	mov			bl, 0x10
	cmp			al, 0x28
	jb			%%SetShrX
	mov			bl, 0x08
	cmp			al, 0x50
	jb			%%SetShrX
	mov			bl, 0x04
	cmp			al, 0x84
	jb			%%SetShrX	   ; Probably never set
	mov			bl, 0x02
%%SetShrX:
	mov			cl, 1
	push			ax
	mov			[MOUSE.DIV.X], bx
	mul			bx
	mov			[MOUSE.MAX.X], ax
	shr			ax, cl
	mov			[MOUSE.VALUE.X], ax
	pop			ax
	shr			ax, cl
	mov			[MOUSE.POSITION.X], ax

	; Figure out Y related values
	; xor			ah, ah	 ; ah should still be 0
	; mov			bh, ah	 ; bh should still be 0
	mov			al, [CRT.Max.Y]   ; Max number of Rows - 1
	mov			bl, 0x10
	cmp			al, 0x19
	jb			%%SetShrY
	mov			bl, 0x08
	cmp			al, 0x2b
	jb			%%SetShrY
	mov			bl, 0x04
	cmp			al, 0x32
	jb			%%SetShrY
	mov			bl, 0x02	   ; Probably never set
%%SetShrY:
	push			ax
	mov			[MOUSE.DIV.Y], bx
	mul			bx
	mov			[MOUSE.MAX.Y], ax
	shr			ax, cl
	mov			[MOUSE.VALUE.Y], ax
	pop			ax
	shr			ax, cl
	mov			[MOUSE.POSITION.Y], ax

	pop			ds, dx, cx, bx, ax
	ret

MouseHide_PROC:
	sub			[MOUSE.CURSOR.STATE], word 1
	cmp			[MOUSE.CURSOR.STATE], word -1
	jne			%%NoStateChange
	push			ax ; this gets pop'd down by %%DoneDrawing
	xor			al, al
	jmp			%%MouseDrawing

MouseShow_PROC:
	add			[MOUSE.CURSOR.STATE], word 1
	cmp			[MOUSE.CURSOR.STATE], word 0
	je			MouseDraw_PROC
%%NoStateChange:
	ret

MouseUndraw_PROC:
	push			ax ; this gets pop'd down by %%DoneDrawing
	cmp			[MOUSE.CURSOR.STATE], word 0x0000
	jl			%%DoneDrawing
	xor			al, al
	jmp			%%MouseDrawing

MouseDraw_PROC:
	push			ax ; this gets pop'd down by %%DoneDrawing
	cmp			[MOUSE.CURSOR.STATE], word 0x0000
	jl			%%DoneDrawing
	mov			al, 0x01
        ; jmp			%%MouseDrawing

%%MouseDrawing:
	; AL = 0/1 for Undraw/Draw
	; Basically save then move cursor to mouse position. Then ether
	; read or write charattr. Then move cursor back to previous position.
	; However, the cursor only actually moves when using BIOS mode.

MouseDrawing:
	test			[MOUSE.BUTTON.COUNT], word 0xffff
	jz			%%DoneDrawing

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		test 	[CRT.Options], byte vcbDirect
		jnz	MouseDrawing_DIRECT
	%endif

%ifdef CRT_BIOS_VIDEO
MouseDrawing_BIOS:

	push			bx, cx, dx
	%ifdef DOS_BUG_FIXES
		; See -- https://fd.lod.bz/rbil/interrup/video/1008.html
		push		si, di, bp
	%endif
	; Save current cursor position
	%ifdef DOS_BUG_FIXES
		; See -- https://fd.lod.bz/rbil/interrup/video/1003.html#122
		push		ax
	%endif
	mov			bh, [CRT.Page]
	mov			ah, 0x03
	int			0x10
	%ifdef DOS_BUG_FIXES
		; See -- https://fd.lod.bz/rbil/interrup/video/1003.html#122
		pop		ax
	%endif
	push			dx
	; Move cursor to mouse position
	mov			dl, [MOUSE.POSITION.X]
	mov			dh, [MOUSE.POSITION.Y]
	; mov			bh, [CRT.Page] ; did this earlier
	mov			ah, 0x02
	int			0x10
	test 			al, al
	jz			%%DoUndraw
%%DoDraw:
	mov			ah, 0x08 ; fetch char/attr
	; mov			bh, [CRT.Page] ; still has not changed
	int			0x10
	mov			[MOUSE.CHARATTR], ax
	not			ah
	and			ah, 0x7f
	jmp			%%SetCharAttr
%%DoUndraw:
	mov			ax, [MOUSE.CHARATTR]
%%SetCharAttr:
	mov			bl, ah
	mov			ah, 0x09
	mov			cx, 0x0001
	; mov			bh, [CRT.Page] ; nope, still the same here
	int			0x10
	; all done, restore cursor position
	pop			dx
	; mov			bh, [CRT.Page] ; give it up, not changing
	mov			ah, 0x02
	int			0x10
	%ifdef DOS_BUG_FIXES
		; See -- https://fd.lod.bz/rbil/interrup/video/1008.html
		push		bp, di, si
	%endif
	pop			dx, cx, bx
%endif

	%ifndef CRT_BIOS_VIDEO
	%elifndef CRT_DIRECT_VIDEO
	%else
		jmp			%%DoneDrawing
	%endif

%ifdef CRT_DIRECT_VIDEO
MouseDrawing_DIRECT:
	push			es, di, dx
	mov			dl, al
	mov			al, [MOUSE.POSITION.X]
	mov			ah, [MOUSE.POSITION.Y]
	CalcVideoPtr		ax ; es:di=video ptr, ax is now bytes per line
	test 			dl, dl  ; write or erase?
	jz			%%DoDirectUndraw
%%DoDirectDraw:
	mov			ax, [es:di]
	mov			[MOUSE.CHARATTR], ax
	not			ah
	and			ah, 0x7f
	jmp			%%SetDirectCharAttr
%%DoDirectUndraw:
	mov			ax, [MOUSE.CHARATTR]
%%SetDirectCharAttr:
	mov			[es:di], ax
	pop			dx, di, es
%endif

%%DoneDrawing:
	pop			ax ; This was pushed a long time back. Like
	ret			   ; circa Mouse(Un)draw_PROC and arrived here
				   ; via horse and carriage.

MouseHandler_INTERRUPT:
; On call
;        AX = Event Flag
;        BX = Button State
;        CX = X coor - ignored
;        DX = Y Coor - ignored
;        SI = X Movement
;        DI = Y Movement

	pushf
	cli
	push			ds, ax, cx, dx, di ;  es, si, bx, not modified

	; Set our data segment
	mov			ds, cs

	; Adjust mouse X position
	mov			ax, [MOUSE.VALUE.X]
	add			ax, si
	cmp			ax, [MOUSE.MIN.X]
	jge			%%NotLowX
	mov			ax, [MOUSE.MIN.X]
%%NotLowX:
	cmp			ax, [MOUSE.MAX.X]
	jle			%%NotHighX
	mov			ax, [MOUSE.MAX.X]
%%NotHighX:
	mov			[MOUSE.VALUE.X], ax
	mov			cx, [MOUSE.DIV.X]
	cwd
	idiv			cx
	push			ax
	; Adjust mouse Y position
	mov			ax, [MOUSE.VALUE.Y]
	add			ax, di
	cmp			ax, [MOUSE.MIN.Y]
	jge			%%NotLowY
	mov			ax, [MOUSE.MIN.Y]
%%NotLowY:
	cmp			ax, [MOUSE.MAX.Y]
	jle			%%NotHighY
	mov			ax, [MOUSE.MAX.Y]
%%NotHighY:
	mov			[MOUSE.VALUE.Y], ax
	mov			cx, [MOUSE.DIV.Y]
	cwd
	idiv			cx
	mov			dx, ax
	pop			cx

	; Test if mouse moved
	cmp			cx, [MOUSE.POSITION.X]
	jne			%%MouseMoved
	cmp			dx, [MOUSE.POSITION.X]
	je			%%MouseNotMoved
%%MouseMoved:
	call			MouseUndraw_PROC
	mov			[MOUSE.POSITION.X], cx
	mov			[MOUSE.POSITION.Y], dx
	call			MouseDraw_PROC
	mov			[MOUSE.FLAG.MOVE], byte 0x01

%%MouseNotMoved:
	cmp			bx, [MOUSE.BUTTON.STATE]
	je			%%NoButtonEvent
	mov			[MOUSE.BUTTON.STATE], bx
	test			bx, bx
	jz			%%NoButtonDown
	mov			[MOUSE.FLAG.RELEASE], word 0
	mov			[MOUSE.BUTTON.LAST], bx
	jmp			%%PutButtonEvent
%%NoButtonDown:
	mov			[MOUSE.FLAG.RELEASE], word 1
%%PutButtonEvent:
	; Button event add to buffer
	mov			di, [MOUSE.BUFFER.HEAD]
	add			di, MOUSE_EVENT_SIZE
	cmp			di, MOUSE.BUFFER.END
	jb			%%BufferNoWrap
	mov			di, MOUSE.BUFFER.START
%%BufferNoWrap:
	cmp			di, [MOUSE.BUFFER.TAIL]
	je			%%BufferFull
	mov			[MOUSE.BUFFER.HEAD], di
	mov			[di], bx
	mov			[di + 2], cl
	mov			[di + 3], dl

	; jmp			%%DoneBufferAdd
%%BufferFull:
	; Could put something here to note buffer hit limit.

%%DoneBufferAdd:

%%NoButtonEvent:
	; reset movement counters
	mov			ax, 0x0b
	int			0x33	; returns CX, DX
	pop			di, dx, cx, ax, ds
	popf			; sti
	retf

%endmacro
; -----------------------------------------------------------------------------
%macro MousePreWrite_INTERNAL 0

MousePreWrite_PROC:
	cli
	push			ax
	test			[MOUSE.BUTTON.COUNT], word 0xffff
	jz			%%NoUndraw
	mov			al, [MOUSE.POSITION.X]
	mov			ah, [MOUSE.POSITION.Y]
	cmp			al, cl
	jb			%%NoUndraw
	cmp			ah, ch
	jb			%%NoUndraw
	cmp			al, dl
	ja			%%NoUndraw
	cmp			ah, dh
	ja			%%NoUndraw
%%DoUndraw:
	mov			[MOUSE.FLAG.REDRAW], byte 1
	call 			MouseUndraw_PROC
	jmp			%%Done

%%NoUndraw:
	mov			[MOUSE.FLAG.REDRAW], byte 0
%%Done:
	pop			ax
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro MousePostWrite_INTERNAL 0

MousePostWrite_PROC:
	cmp			[MOUSE.FLAG.REDRAW], byte 0
	je			%%Done
	call 			MouseDraw_PROC
	mov			[MOUSE.FLAG.REDRAW], byte 0
%%Done:
	sti
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro MouseMoved_INTERNAL 0

MouseMoved_PROC:
	test			[MOUSE.FLAG.MOVE], byte 0xff
	clc
	jz			%%Done
	stc
%%Done:
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro MouseMoveRead_INTERNAL 0

MouseMoveRead_PROC:
	mov			dl, [MOUSE.POSITION.X]
	mov			dh, [MOUSE.POSITION.Y]
	test			[MOUSE.FLAG.MOVE], byte 0xff
	clc
	jz			%%Done
	mov			[MOUSE.FLAG.MOVE], byte 0x00
	stc
%%Done:
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro MouseClicked_INTERNAL 0

MouseClicked_PROC:
	push			ax
	mov			ax, [MOUSE.BUFFER.HEAD]
	cmp			[MOUSE.BUFFER.TAIL], ax
	clc
	je			%%Done
	stc
%%Done:
	pop			ax
	ret

%endmacro
; -----------------------------------------------------------------------------
%macro MouseClickRead_INTERNAL 0
MouseClickRead_PROC:
	cli
	push			si
	mov			si, [MOUSE.BUFFER.TAIL]
	cmp			si, [MOUSE.BUFFER.HEAD]
	je			%%NoEvent
	add			si, MOUSE_EVENT_SIZE
	cmp			si, MOUSE.BUFFER.END
	jb			%%BufferNoWrap
	mov			si, MOUSE.BUFFER.START
%%BufferNoWrap:
	mov			[MOUSE.BUFFER.TAIL], si
	mov			bx, [si]
	mov			dl, [si + 2]
	mov			dh, [si + 3]
	stc
	jmp			%%Done
%%NoEvent:
	mov			bx, [MOUSE.BUTTON.STATE]
	mov			dl, [MOUSE.POSITION.X]
	mov			dh, [MOUSE.POSITION.Y]
	clc
%%Done:
	pop			si
	sti
	ret
%endmacro
; -----------------------------------------------------------------------------
%macro MouseWhereXY_INTERNAL 0
MouseWhereXY_PROC:
	cli
	push			si
	mov			si, [MOUSE.BUFFER.TAIL]
	cmp			si, [MOUSE.BUFFER.HEAD]
	je			%%NoEvent
	add			si, MOUSE_EVENT_SIZE
	cmp			si, MOUSE.BUFFER.END
	jb			%%BufferNoWrap
	mov			si, MOUSE.BUFFER.START
%%BufferNoWrap:
;	mov			[MOUSE.BUFFER.TAIL], si
	mov			bx, [si]
	mov			dl, [si + 2]
	mov			dh, [si + 3]
	stc
	jmp			%%Done
%%NoEvent:
	mov			bx, [MOUSE.BUTTON.STATE]
	mov			dl, [MOUSE.POSITION.X]
	mov			dh, [MOUSE.POSITION.Y]
	clc
%%Done:
	pop			si
	sti
	ret
%endmacro
; -----------------------------------------------------------------------------
%macro MouseClickPurge_INTERNAL 0
MouseClickPurge_PROC:
	push		bx, dx
%%Purge:
	MouseClickRead	bx, dx
	jc		%%Purge
	xor		bx, bx
	mov		[MOUSE.BUTTON.LAST], bx
	mov		[MOUSE.FLAG.RELEASE], bx
	pop		dx, bx
	ret
%endmacro
; -----------------------------------------------------------------------------
%macro MouseAvailable_INTERNAL 0
MouseAvailable_PROC:
	test		[MOUSE.BUTTON.COUNT], word 0xffff
	clc
	jz		%%NoMouse
	stc
%%NoMouse:
	ret
%endmacro

; *****************************************************************************
%ifdef CRT_MOUSE
; *****************************************************************************
; These are handled by Core CRT, don't worry about them.
; *****************************************************************************
REQUIRE		MouseSupportProcs
REQUIRE		MouseSupportData
REQUIRE		MousePreWrite
REQUIRE		MousePostWrite
; -----------------------------------------------------------------------------
%imacro MouseInitialize 0
	call 		MouseInitialize_PROC
	test		[CRT.Options], word vcbAutoMouse
	jz		%%NoAutoMouse
	call 		MouseShow_PROC
%%NoAutoMouse:
%endmacro
; -----------------------------------------------------------------------------
%imacro MouseFinalize 0
	call 		MouseFinalize_PROC
%endmacro
; -----------------------------------------------------------------------------
%imacro MousePreWrite 0-2
	%if %0 = 0
		push	cx, dx
		mov	cx, [CRT.Wind.Min]
		mov	dx, [CRT.Wind.Max]
		call	MousePreWrite_PROC
		pop	dx, cx
	%elif %0 = 1
		push	dx
		pushndr cx, %1
		setndr	cx, %1
		mov	dx, cx
		call	MousePreWrite_PROC
		popndr	cx, %1
		pop	dx
	%else
		pushndr cx, %1, dx, %2
		setndr	cx, %1, dx, %2
		call	MousePreWrite_PROC
		popndr cx, %1, dx, %2
	%endif
%endmacro
; -----------------------------------------------------------------------------
%imacro MousePostWrite 0
	call			MousePostWrite_PROC
%endmacro
; *****************************************************************************
; -----------------------------------------------------------------------------
; End User / Programmer / Developer Functions
; -----------------------------------------------------------------------------
; *****************************************************************************
%imacro MouseDataReset 0
	call	MouseDataReset_PROC
%endmacro
; *****************************************************************************
%imacro MouseShow 0
	call 	MouseShow_PROC
%endmacro
; *****************************************************************************
%imacro MouseHide 0
	call 	MouseHide_PROC
%endmacro
; *****************************************************************************
; Test if mouse has been moved
; MACRO:  MouseMoved 0-1
; INPUT:  %1 = [Optional] jump address if mouse has been moved.
; OUTPUT: Carry Flag Set if mouse has moved. Does not clear movement flag.
; *****************************************************************************
%imacro MouseMoved 0-1
	REQUIRE		MouseMoved
	call		MouseMoved_PROC
	%if %0 = 1
		jc	%1
	%endif
%endmacro
; *****************************************************************************
; Test if mouse has not moved
; MACRO:  MouseNotMoved 0-1
; INPUT:  %1 = [Optional] jump address if mouse has not moved.
; OUTPUT: Carry Flag Set if mouse has moved. (Same as MouseMoved)
; *****************************************************************************
%imacro MouseNotMoved 0-1
	REQUIRE		MouseMoved
	call		MouseMoved_PROC
	%if %0 = 1
		jnc	%1
	%endif
%endmacro
; *****************************************************************************
; Check if mouse has moved and get position.
; MACRO:  MouseMoveRead 1
; INPUT:  none
; OUTPUT:
;	%1 = current mouse screen position (prefers dx)
;	Carry Flag Set if mouse has moved since last ReadMouseMove.
; *****************************************************************************
%imacro MouseMoveRead 1
	REQUIRE		MouseMoveRead
	PushNDR		dx, %1
	call		MouseMoveRead_PROC
	SetNDR		%1, dx
	PopNDR		dx, %1
%endmacro
; *****************************************************************************
; Check if mouse was clicked and button has been released.
; MACRO:  MouseReleased 1
; INPUT:  %1 = [Optional] jump address if a mouse button release event occurred.
; OUTPUT:
;	Carry Flag Set when true. Button release FLAG is cleared.
; *****************************************************************************
%imacro MouseReleased 1
	cmp		[MOUSE.FLAG.RELEASE], word 0
	je		%%NotReleased
	mov		[MOUSE.FLAG.RELEASE], word 0
	stc
	%if %0 = 1
		jmp	%1
	%else
		jmp	%%Done
	%endif
%%NotReleased:
	clc
%%Done:
%endmacro
; *****************************************************************************
; Test if any mouse button event has occurred and is waiting in the buffer.
; MACRO:  MouseClicked 0-1
; INPUT:  %1 = [Optional] jump address if a mouse button event occurred.
; OUTPUT: Carry Flag Set if mouse has button event is waiting in the buffer.
; *****************************************************************************
%imacro MouseClicked 0-1
	REQUIRE		MouseClicked
	call		MouseClicked_PROC
	%if %0 = 1
		jc	%1
	%endif
%endmacro
; *****************************************************************************
; Test if no mouse button events are waiting in the buffer.
; MACRO:  MouseNotClicked 0-1
; INPUT:  %1 = [Optional] jump address if a mouse button event has not occurred.
; OUTPUT: Carry Flag Set if a button event is waiting. (Same as MouseClicked)
; *****************************************************************************
%imacro MouseNotClicked 0-1
	REQUIRE		MouseClicked
	call		MouseClicked_PROC
	%if %0 = 1
		jnc	%1
	%endif
%endmacro
; *****************************************************************************
; Check if mouse button event has occurred and remove it from the buffer.
; Generally, this is the only mouse macro you will absolutely need to use.
; However, if you want to do things like hover-over highlighting. You will
; also may to use the MouseMove/ReadMouseMove macros to simplify the code.
; MACRO:  MouseClickRead 2
; INPUT:  none
; OUTPUT:
;	If CarryFlag is true, then...
;	%1 = mouse button status for event (prefers bx)
;	%2 = mouse screen position for event (prefers dx)
;	If Carry Flag is false, then...
;	%1 = current mouse button status (prefers bx)
;	%2 = current mouse screen position (prefers dx)
; *****************************************************************************
%imacro MouseClickRead 2

	REQUIRE		MouseClickRead
	PushNDR		bx, %1, dx, %2
	call		MouseClickRead_PROC
	SetNDR		%1, bx, %2, dx
	PopNDR		bx, %1, dx, %2

%endmacro
; *****************************************************************************
; Very similar to MouseClickRead, but position (not button) focused.
; It is a Non-destruction read of mouse position (and buttons) events.
; MACRO:  MouseWhereXY 1-2
; INPUT:  none
; OUTPUT:
;	If CarryFlag is true, then...
;	%1 = mouse screen position for event (prefers dx)
;	%2 = (optional) mouse button status for event (prefers bx)
;	If Carry Flag is false, then...
;	%1 = current mouse screen position (prefers dx)
;	%2 = (optional) current mouse button status (prefers bx)
; *****************************************************************************
%imacro MouseWhereXY 1-2

	REQUIRE		MouseWhereXY
	%if %0 = 2
		PushNDR		dx, %1, bx, %2
		call		MouseWhereXY_PROC
		SetNDR		%1, dx, %2, bx
		PopNDR		dx, %1, bx, %2
	%else
		PushNDR		dx, %1
		push		bx
		call		MouseWhereXY_PROC
		pop		bx
		SetNDR		%1, dx
		PopNDR		dx, %1
	%endif

%endmacro
; *****************************************************************************
; Discard any and all mouse clicks that may be waiting in the buffer.
; MACRO:  MousePurge 0
; INPUT:  none
; OUTPUT: none
; *****************************************************************************
%imacro MouseClickPurge 0
	REQUIRE		MouseClickPurge
	call 		MouseClickPurge_PROC
%endmacro
%idefine MousePurge MouseClickPurge
; *****************************************************************************
; Test if mouse was found and initialized.
; MACRO:  MouseAvailable 0-1
; OUTPUT: Carry Flag set if Mouse was found. If mouse is available, %1 can be
;	  provided as a jump target.
; *****************************************************************************
%imacro MouseAvailable 0-1

	REQUIRE		MouseAvailable
	call		MouseAvailable_PROC
	%if %0 = 1
		jnc		%%NoMouse
		jmp		%1
		%%NoMouse:
	%endif

%endmacro
; *****************************************************************************
%else ; %ifdef CRT_MOUSE
; *****************************************************************************
%macro NO_MOUSE_SUPPORT 0-*
%endmacro

%idefine MouseInitialize 	NO_MOUSE_SUPPORT
%idefine MouseFinalize 		NO_MOUSE_SUPPORT
%idefine MousePreWrite 		NO_MOUSE_SUPPORT
%idefine MousePostWrite		NO_MOUSE_SUPPORT

%idefine MouseDataReset 	NO_MOUSE_SUPPORT
%idefine MouseHide	 	NO_MOUSE_SUPPORT
%idefine MouseShow	 	NO_MOUSE_SUPPORT
%idefine MouseMoved		clc
%imacro MouseNotMoved 0-1
	clc
	%if %0 = 1
		jmp	%1
	%endif
%endmacro
%imacro MouseMoveRead 1
	clc
	mov	%1, word 0
%endmacro
%idefine MouseClicked		clc
%imacro MouseNotClicked 0-1
	clc
	%if %0 = 1
		jmp	%1
	%endif
%endmacro
%imacro MouseClickRead 2
	clc
	mov	%1, word 0
	mov	%2, word 0
%endmacro
%idefine MouseClickPurge 	NO_MOUSE_SUPPORT
%idefine MousePurge	 	NO_MOUSE_SUPPORT
%idefine MouseAvailable 	clc
%idefine MouseReleased		clc
; *****************************************************************************
%endif ; %ifdef CRT_MOUSE
; *****************************************************************************
%else				; if file has already been included
; *****************************************************************************

%ifdef CRT_MOUSE
	PUSH_SECTION .proc
		PROVIDE		MouseSupportProcs
		PROVIDE		MousePreWrite
		PROVIDE		MousePostWrite
		PROVIDE 	MouseMoved
		PROVIDE		MouseMoveRead
		PROVIDE		MouseClicked
		PROVIDE		MouseClickRead
		PROVIDE		MouseClickPurge
		PROVIDE		MouseAvailable
		PROVIDE		MouseWhereXY
	POP_SECTION

	PROVIDE_BSS	MouseSupportData

%endif

; *****************************************************************************

%endif
