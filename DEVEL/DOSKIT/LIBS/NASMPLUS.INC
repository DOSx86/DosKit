; Doskit application libraries
; Assembly language extensions and pseudo instructions for NASM

; BSD 3-Clause License
; Copyright (c) 2022-2023, Jerome Shidel

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:

; 1. Redistributions of source code must retain the above copyright notice, this
;    list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above copyright notice,
;    this list of conditions and the following disclaimer in the documentation
;    and/or other materials provided with the distribution.

; 3. Neither the name of the copyright holder nor the names of its
;    contributors may be used to endorse or promote products derived from
;    this software without specific prior written permission.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
; CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
; OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

; NASM 2.15.05, or later

%define NASM_WARNING

%if __?NASM_MAJOR?__ >= 2
	%if __?NASM_MINOR?__ > 15
		%undef NASM_WARNING
	%elif __?NASM_MINOR?__ = 15
		%if __?NASM_SUBMINOR?__ >= 5
			%undef NASM_WARNING
		%endif
	%endif
%endif
%ifdef NASM_WARNING
	%error Requires NASM 2.15.05, or later
%endif
; *****************************************************************************
%ifndef NASMPLUS_INC_DEFINED 	; check that this file has not been included
%define NASMPLUS_INC_DEFINED
; *****************************************************************************
; As a rule, macros in this include that are declared as case sensitive (just
; using %macro) are not intended to be generally used. They are primarily
; internal macros.
; *****************************************************************************
; Switch to a different code section. The first time per section, it will
; insert 4 NOPs at the start of the section. This will ensure that disassembles
; can correctly disassemble the code in that section. This is only necessary
; when you want to ensure such compatibility.
; *****************************************************************************

; *****************************************************************************
%imacro REQUIRE 1
	%ifndef REQUIRE_%1
		%define REQUIRE_%1 0
		;%warning Requiring %1
	%endif
%endmacro

%imacro PROVIDE 1
	%ifdef REQUIRE_%1
		%ifidni REQUIRE_%1, 0
			%1_INTERNAL
			%define REQUIRE_%1 1
			;%warning Providing %1
		%endif
	%endif
%endmacro

; *****************************************************************************
%imacro ALIGNED_SECTION 1
%ifdef USE_SECTIONS
	%idefine SECTION_ID %1
	%ifndef SECTION_%1_DEFINED
	%define SECTION_%1_DEFINED 1
		%ifndef SECTION_ALIGNMENT
			; undefined by developer or DEFAULTS.INC not included
			; do not stick them NOPs in the code, alignment based
			; on current alignment setting will still occur
			section %1
		%elif SECTION_ALIGNMENT = 0
			; do not stick them NOPs in the code, alignment based
			; on current alignment setting will still occur
			section %1
		%else
			section %1
			align SECTION_ALIGNMENT
			%ifdef SECTION_NOPS
				; times SECTION_NOPS nop
			%endif
		%endif
	%else
		; section already exists previously. just switch to it now
		section %1
	%endif
%endif
; __?SECT?__
%endmacro
; *****************************************************************************
%idefine IsRegPair(x) (%substr(%str(x),3,1) == ':')
%idefine NotRegPair(x) (%substr(%str(x),3,1) != ':')
%idefine RegPairHigh(x) (%tok(%substr(%str(x),1,2)))
%idefine RegPairLow(x) (%tok(%substr(%str(x),4,-1)))

%idefine IsReg8(x) 	((%str(x) == 'al') || (%str(x) == 'ah') || \
			 (%str(x) == 'bl') || (%str(x) == 'bh') || \
			 (%str(x) == 'cl') || (%str(x) == 'ch') || \
			 (%str(x) == 'dl') || (%str(x) == 'dh') || \
			 (%str(x) == 'AL') || (%str(x) == 'AH') || \
			 (%str(x) == 'BL') || (%str(x) == 'BH') || \
			 (%str(x) == 'CL') || (%str(x) == 'CH') || \
			 (%str(x) == 'DL') || (%str(x) == 'DH'))

%idefine IsReg16(x) 	((%str(x) == 'ax') || (%str(x) == 'bx') || \
			 (%str(x) == 'cx') || (%str(x) == 'dx') || \
			 (%str(x) == 'ds') || (%str(x) == 'si') || \
			 (%str(x) == 'es') || (%str(x) == 'di') || \
			 (%str(x) == 'ss') || (%str(x) == 'sp') || \
			 (%str(x) == 'cs') || (%str(x) == 'ip') || \
			 (%str(x) == 'bp') || (%str(x) == 'BP') || \
			 (%str(x) == 'AX') || (%str(x) == 'BX') || \
			 (%str(x) == 'CX') || (%str(x) == 'DX') || \
			 (%str(x) == 'DS') || (%str(x) == 'SI') || \
			 (%str(x) == 'ES') || (%str(x) == 'DI') || \
			 (%str(x) == 'SS') || (%str(x) == 'SP') || \
			 (%str(x) == 'CS') || (%str(x) == 'IP'))
; *****************************************************************************
; For internal use to actually get PUSH/POP Sections to work.
; -----------------------------------------------------------------------------
%macro PUSH_SECTION_INTERNAL 1
	%xdefine PUSH_SECTION_ITEM%1 __?SECT?__
%endmacro

%macro POP_SECTION_INTERNAL 1
	%xdefine __?SECT?__ PUSH_SECTION_ITEM%1
	%ifdef USE_SECTIONS
		PUSH_SECTION_ITEM%1
	%endif
	; %warning SECTION POP PUSH_SECTION_ITEM%1
	%undef PUSH_SECTION_ITEM%1
%endmacro

%imacro PROVIDE_BSS 1

	%ifdef USE_SECTIONS
		PUSH_SECTION .bss
	%else
		[section .bss]
	%endif

	PROVIDE		%1

	%ifdef USE_SECTIONS
		POP_SECTION
	%else
		__?SECT?__
	%endif

%endmacro
; *****************************************************************************
; Works like a command line directory stack... but for code segments.
; -----------------------------------------------------------------------------
%imacro PUSH_SECTION 1
	%ifndef PUSH_SECTION_COUNTER
		%assign PUSH_SECTION_COUNTER 0
	%else
		%assign PUSH_SECTION_COUNTER PUSH_SECTION_COUNTER + 1
	%endif
	PUSH_SECTION_INTERNAL PUSH_SECTION_COUNTER
	%ifdef USE_SECTIONS
		ALIGNED_SECTION %1
	%endif
	; %warning SECTION PUSH %1
%endmacro

%imacro POP_SECTION 0
	%ifndef PUSH_SECTION_COUNTER
		%error cannot pop section, no previous section on stack
		%ifdef USE_SECTIONS
			section .text
		%endif
	%else
		POP_SECTION_INTERNAL PUSH_SECTION_COUNTER
		%if PUSH_SECTION_COUNTER = 0
			%undef PUSH_SECTION_COUNTER
		%else
			%assign PUSH_SECTION_COUNTER PUSH_SECTION_COUNTER - 1
		%endif
	%endif
%endmacro
; *****************************************************************************
; determines bit size of a register parameter
%imacro REG_BITS 1
	%undef REG_16
	%undef REG_8

	%ifidni %1, ax
		%define REG_16 ax
	%elifidni %1, bx
		%define REG_16 bx
	%elifidni %1, cx
		%define REG_16 cx
	%elifidni %1, dx
		%define REG_16 dx
	%elifidni %1, si
		%define REG_16 si
	%elifidni %1, di
		%define REG_16 di
	%elifidni %1, bp
		%define REG_16 bp
	%elifidni %1, sp
		%define REG_16 sp
	%elifidni %1, es
		%define REG_16 es
	%elifidni %1, ds
		%define REG_16 ds
	%elifidni %1, cs
		%define REG_16 cs
	%elifidni %1, ss
		%define REG_16 ss
	%elifidni %1, ah
		%define REG_8 ah
	%elifidni %1, al
		%define REG_8 al
	%elifidni %1, bh
		%define REG_8 bh
	%elifidni %1, bl
		%define REG_8 bl
	%elifidni %1, ch
		%define REG_8 ch
	%elifidni %1, cl
		%define REG_8 cl
	%elifidni %1, dh
		%define REG_8 dh
	%elifidni %1, dl
		%define REG_8 dl
	%endif

%endmacro

; determines the size and type of a macro parameter.
%imacro REG_TYPE 1
	%undef REG_CLASS
	%undef REG_VALUE
	%undef REG_TEXT
	%undef REG_TOKEN
 	%undef REG_PAIR
	%undef REG_PAIR_HIGH
	%undef REG_PAIR_LOW

	%ifnum %1
		%define REG_VALUE %1
		%define REG_CLASS VALUE
	%elifstr %1
		%define REG_TEXT %1
		%define REG_CLASS TEXT
	%elif %strlen(%str(%1)) == 5
		%if %substr(%str(%1),3,1) == ':'
			%deftok REG_PAIR_HIGH %substr(%str(%1),1,2)
			REG_BITS REG_PAIR_HIGH
			%ifndef REG_16
				%undef REG_PAIR_HIGH
			%else
				%deftok REG_PAIR_LOW %substr(%str(%1),4,2)
				REG_BITS REG_PAIR_LOW
				%ifndef REG_16
					%undef REG_PAIR_HIGH
					%undef REG_PAIR_LOW
				%endif
			%endif
			%undef REG_16
			%undef REG_8
			%ifdef REG_PAIR_HIGH
				%define REG_CLASS PAIR
				%define REG_PAIR REG_PAIR_HIGH:REG_PAIR_LOW
			%endif
		%endif
	%endif
	%ifndef REG_CLASS
		REG_BITS %1
		%ifdef REG_16
			%define REG_CLASS 16
		%elifdef REG_8
			%define REG_CLASS 8
		%elif %substr(%str(%1),1,1) == '['
			%define REG_CLASS MEMORY
			%define REG_MEMORY %1
		%else
			%define REG_CLASS TOKEN
			%define REG_TOKEN %1
		%endif
	%endif
%endmacro

%imacro REG_TEMP_SUBMAC 1
	%ifidni %1, ax
		%undef  REGISTER_AX_AVAILABLE
	%elifidni %1, al
		%undef  REGISTER_AX_AVAILABLE
	%elifidni %1, ah
		%undef  REGISTER_AX_AVAILABLE
	%elifidni %1, bx
		%undef  REGISTER_BX_AVAILABLE
	%elifidni %1, bl
		%undef  REGISTER_BX_AVAILABLE
	%elifidni %1, bh
		%undef  REGISTER_BX_AVAILABLE
	%elifidni %1, cx
		%undef  REGISTER_CX_AVAILABLE
	%elifidni %1, cl
		%undef  REGISTER_CX_AVAILABLE
	%elifidni %1, ch
		%undef  REGISTER_CX_AVAILABLE
	%elifidni %1, dx
		%undef  REGISTER_DX_AVAILABLE
	%elifidni %1, dl
		%undef  REGISTER_DX_AVAILABLE
	%elifidni %1, dh
		%undef  REGISTER_DX_AVAILABLE
	%elifidni %1, si
		%undef  REGISTER_DI_AVAILABLE
	%elifidni %1, di
		%undef  REGISTER_SI_AVAILABLE
	%endif
%endmacro

%imacro REG_TEMP 1-*
	%undef   TEMP_REGISTER
	%define  REGISTER_AX_AVAILABLE
	%define  REGISTER_BX_AVAILABLE
	%define  REGISTER_CX_AVAILABLE
	%define  REGISTER_DX_AVAILABLE
	%define  REGISTER_SI_AVAILABLE
	%define  REGISTER_DI_AVAILABLE

	%rep  %0
		%if %substr(%str(%1),3,1) == ':'
			REG_TEMP_SUBMAC %tok(%substr(%str(%1),1,2))
			REG_TEMP_SUBMAC %tok(%substr(%str(%1),4,2))
		%elif %substr(%str(%1),1,1) == '['
			REG_TEMP_SUBMAC %tok(%substr(%str(%1),2,2))
			REG_TEMP_SUBMAC %tok(%substr(%str(%1),5,2))
		%else
			REG_TEMP_SUBMAC %1
		%endif
		%rotate 1
	%endrep

	%ifdef REGISTER_AX_AVAILABLE
		%define TEMP_REGISTER ax
	%elifdef REGISTER_BX_AVAILABLE
		%define TEMP_REGISTER bx
	%elifdef REGISTER_CX_AVAILABLE
		%define TEMP_REGISTER cx
	%elifdef REGISTER_DX_AVAILABLE
		%define TEMP_REGISTER dx
	%elifdef REGISTER_SI_AVAILABLE
		%define TEMP_REGISTER si
	%elifdef REGISTER_DI_AVAILABLE
		%define TEMP_REGISTER di
	%else
		%error All registers in use, cannot select a temporary register
	%endif
	%undef  REGISTER_AX_AVAILABLE
	%undef  REGISTER_BX_AVAILABLE
	%undef  REGISTER_CX_AVAILABLE
	%undef  REGISTER_DX_AVAILABLE
	%undef  REGISTER_DI_AVAILABLE
	%undef  REGISTER_SI_AVAILABLE

%endmacro
; -----------------------------------------------------------------------------
; Push, Pop and Set when Not Default Register. Paramaters are provided in pairs.
; First is the Default Register (or Register PAIR), second is the macro parameter
; number. Paramaters for PushNDR and PopNDR must always be given in the same
; order with the same contents. There basic usage will look something like this:
;
; %imacro PrintDemo 3		; %1 is pointer, %2 is width, %3 is height
; 	PushNDR   es:di, %1, cx, %2, dx, %3
; 	SetNDR    es:di, %1, dx, %3, cx, %2	; fine to shuffle them in SetNDR
;	push	  ax		; printer function trashes it
; 	call      printer	; es:di->ASciiZ, cx=width, dx=height
;	pop	  ax		; restored after printer function
;	SetNDR	  %1, es:di	; update pointer to return end of string
; 	PopNDR    es:di, %1, cx, %2, dx, %3	; must be identical to PushNDR
; %endmacro
;
; PrintDemo es:di, cx, dx	; Print First String
; PrintDemo es:di, ax, bx	; Print Next String
; ...
; PrintDemo ds:si, cx, bx	; Print Other First Message
; PrintDemo ds:si, cx, bx	; Print Other Next Message
;
; Generally when calling a macro, you should use it's default registers if
; possible. Otherwise, a lot of extra code might be generated to juggle things
; around. Also, Return paramater can get a bit iffy.  For example, this would be
; a problem... "PrintDemo dx:ax, cx, bx" Why, DX:AX would be set to ES:DI? Well,
; sure. But, then we get to PopNDR, pop es:di fine, cx is ignored. But, then we
; get to %3. Internally, it is DX, not BX. So, it was saved in PushNDR and gets
; restored in PopNDR and trashes the return value in SetNDR. It's a trade off
; in ease of programming and code bloat. Maybe I'll improve the process someday.
;
; Just use the default registers, unless you really need to do it another way.
; -----------------------------------------------------------------------------
%imacro PushNDR 2-*
	%if %0 / 2 * 2 != %0
		%fatal requires an even number of paramaters, PUSHNDR %{1:-1}
	%endif
	%rep %0 / 2
		; %warning push %1
		%ifnidni %1,%2
			; %if %strlen(%substr(%str(%1),3,1)) = ':'

			; maybe better to add a PUSH/POP for half registers.
			; Probably better. I'll think about it. Maybe later.
			%ifidni %1, al
				push 	ax
			%elifidni %1, ah
				push 	ax
			%elifidni %1, bl
				push 	bx
			%elifidni %1, bh
				push 	bx
			%elifidni %1, cl
				push 	cx
			%elifidni %1, ch
				push 	cx
			%elifidni %1, dl
				push 	dx
			%elifidni %1, dh
				push 	dx
			%else
		        	%if (%substr(%str(%1),3,1) = ':') && (%substr(%str(%2),3,1) != ':')
		        		%fatal register PAIR required
		        	%elif (%substr(%str(%1),3,1) != ':') && (%substr(%str(%2),3,1) = ':')
		        		%fatal register PAIR provided, other register type required
		        	%endif
				push	%1
			%endif
			; %endif
		%endif
		%rotate 2
	%endrep
%endmacro

%imacro PopNDR 2-*
	%if %0 / 2 * 2 != %0
		%fatal requires an even number of paramaters, POPNDR %{1:-1}
	%endif
	%rep %0 / 2
		%rotate -2
		; %warning pop %1
		%ifnidni %1,%2
			%ifidni %1, al
				pop 	ax
			%elifidni %1, ah
				pop 	ax
			%elifidni %1, bl
				pop 	bx
			%elifidni %1, bh
				pop 	bx
			%elifidni %1, cl
				pop 	cx
			%elifidni %1, ch
				pop 	cx
			%elifidni %1, dl
				pop 	dx
			%elifidni %1, dh
				pop 	dx
			%else
		        	%if (%substr(%str(%1),3,1) = ':') && (%substr(%str(%2),3,1) != ':')
		        		%fatal register PAIR required
		        	%elif (%substr(%str(%1),3,1) != ':') && (%substr(%str(%2),3,1) = ':')
		        		%fatal register PAIR provided, other register type required
		        	%endif
				pop	%1
			%endif
		%endif
	%endrep

%endmacro

%imacro SetNDR 2-*
	%ifidni %0, 2
	; Only 2 params
    		%ifnidni %1, %2
			; SetNDR ax, bx
			mov %1, %2
		%endif
	%elifidni (%0, 4) && (%substr(%str(%1),3,1) != ':') && (%substr(%str(%3),3,1) != ':')
	; only 4 params and neither destination is a word pair
		%ifidni %1, %4
			; SetNDR  ax, bx, cx, ax
			; SetNDR  ax, bx, bx, ax
			push %4
			mov  %1, %2
			pop  %3
		%else
			%ifnidni %1, %2
				mov %1, %2
			%endif
			%ifnidni %3, %4
				mov %3, %4
			%endif
		%endif
	%else
	; %warning 6+ paramater mode
		%undef TEMP_REGISTER
		; check if any parameter is a number. if so we will need a
		; scratch register.
		%rep  %0
			%ifnum %2
				%define TEMP_REGISTER
			%endif
			%rotate 2
		%endrep
		%ifdef TEMP_REGISTER
			; get a temporary scratch register and save it's contents
			REG_TEMP %{1:-1}
			push TEMP_REGISTER
			; %warning using Temporary Register TEMP_REGISTER
		%endif
		; push each non-default register/number onto stack
	        %rep  %0 / 2
			%ifnidni %1, %2
			    	%ifnum %2
			        	%if (%substr(%str(%1),3,1) = ':')
			        		%fatal register PAIR required
			        	%endif
					mov  TEMP_REGISTER, %2
					push TEMP_REGISTER
				%else
			        	; %warning push %2
			        	%if (%substr(%str(%1),3,1) = ':') && (%substr(%str(%2),3,1) != ':')
			        		%fatal register PAIR required
			        	%elif (%substr(%str(%1),3,1) != ':') && (%substr(%str(%2),3,1) = ':')
			        		%fatal register PAIR provided, other register type required
			        	%endif
					push %2
				%endif
			%endif
			%rotate 2
	        %endrep
	        ; pop each non-default register from stack
		%rep %0 / 2
			%rotate -2
			%ifnidni %1, %2
			    	%ifnum %2
			        	; %warning pop %1 using TEMP_REGISTER
					pop  TEMP_REGISTER
					mov  %1, TEMP_REGISTER
				%else
			        	; %warning pop %1
					pop %1
				%endif
			%endif
		%endrep
		; restore temporary register if used
		%ifdef TEMP_REGISTER
		    	pop TEMP_REGISTER
		%endif
		%undef TEMP_REGISTER
	%endif

%endmacro

%imacro PushSRS 1-*
	%ifdef SAVE_REGISTER_STATE
		push %{1:-1}
	%endif
%endmacro

%imacro PopSRS 1-*
	%ifdef SAVE_REGISTER_STATE
		pop %{1:-1}
	%endif
%endmacro

; *****************************************************************************
; Basic language extensions
; *****************************************************************************
%imacro PUSH_WORD 1
; work around, "PUSH WORD ????" is not part of the 8086 instruction set
	push 	ax		; going to overwrite value saved by AX
	push	bp
	mov	bp,sp
	mov	[ss:bp+2], word %1
	pop	bp
%endmacro

%imacro PUSH_ALL 0
	pushf
	push	ax
	push 	bx
	push 	cx
	push	dx
	push	si
	push	di
	push	es
	push	ds
	push	bp
%endmacro

%imacro POP_ALL 0
	pop	bp
	pop	ds
	pop	es
	pop	di
	pop	si
	pop	dx
	pop	cx
	pop	bx
	pop	ax
	popf
%endmacro
; -----------------------------------------------------------------------------
; Multiple register PUSH
; -----------------------------------------------------------------------------
%imacro PUSH 1-*
	%rep %0
		%if %substr(%str(%1),3,1) == ':'
			push %tok(%substr(%str(%1),1,2))
			push %tok(%substr(%str(%1),4,-1))
		%else
			%ifidni %1, al
				%fatal PUSH does not support 8-bit register %1
			%elifidni %1, ah
				%fatal PUSH does not support 8-bit register %1
			%elifidni %1, bl
				%fatal PUSH does not support 8-bit register %1
			%elifidni %1, bh
				%fatal PUSH does not support 8-bit register %1
			%elifidni %1, cl
				%fatal PUSH does not support 8-bit register %1
			%elifidni %1, ch
				%fatal PUSH does not support 8-bit register %1
			%elifidni %1, dl
				%fatal PUSH does not support 8-bit register %1
			%elifidni %1, dh
				%fatal PUSH does not support 8-bit register %1
			%endif
			push %1
		%endif
		%rotate 1
	%endrep
%endmacro
; -----------------------------------------------------------------------------
; Multiple register POP
; -----------------------------------------------------------------------------
%imacro POP 1-*
	%rep %0
		%if %substr(%str(%1),3,1) == ':'
			pop %tok(%substr(%str(%1),4,-1))
			pop %tok(%substr(%str(%1),1,2))
		%else
			pop %1
		%endif
		%rotate 1
	%endrep
%endmacro
; -----------------------------------------------------------------------------
%imacro MOV_BASIC_SUBMAC 2
	; %warning %1 %2
	%ifidni %1, cs
		%error invalid instruction MOV %1, %2
	%elifidni %1, ip
		%error invalid instruction MOV %1, %2
	%elifidni %1, es
		%ifnum %2
			push ax
			mov  ax, %2
			push ax
			pop  es
			pop  ax
		%elif %substr(%str(%2),1,1) == '['
			push ax
			mov  ax, %2
			push ax
			pop  es
			pop  ax
		%else
			push %2
			pop  es
		%endif
	%elifidni %1, ds
		%ifnum %2
			push ax
			mov  ax, %2
			push ax
			pop  ds
			pop  ax
		%elif %substr(%str(%2),1,1) == '['
			push ax
			mov  ax, %2
			push ax
			pop  ds
			pop  ax
		%else
			push %2
			pop  ds
		%endif
	%elifidni %2, ip
		call %%ShortCall
		%%ShortCall:
		pop	 %1
	%else
		mov	 %1, %2
	%endif

%endmacro
; -----------------------------------------------------------------------------
; MOV operator extension
; extended mov to permit the following:
; mov ES, %2
; mov DS, %2
; mov %1, IP
; mov bx:cx,dx:ax ,etc
; -----------------------------------------------------------------------------
%imacro MOV 2
	%if %substr(%str(%1),3,1) == ':'	; to register pair
		%if %substr(%str(%2),3,1) == ':'	; from register pair
			%if %substr(%str(%1),1,2) == %substr(%str(%2),4,2) || %substr(%str(%2),1,2) == %substr(%str(%1),4,2)
				; collision like, mov ax:bx, bx:ax
				; however, mov dx:ax, ds:axis will trigger as well

				push	%2
				pop	%1
			%else
				MOV_BASIC_SUBMAC %tok(%substr(%str(%1),1,2)), %tok(%substr(%str(%2),1,2))
				MOV_BASIC_SUBMAC %tok(%substr(%str(%1),4,-1)), %tok(%substr(%str(%2),4,-1))
			%endif
		%elif %substr(%str(%2),1,1) == '['	; from memory
			%error instruction not implemented, MOV %1, %2
		%else
			%error invalid combination of opcode and operands, MOV %1, %2
		%endif
	%elif %substr(%str(%2),3,1) == ':'	; from register pair
		%if %substr(%str(%1),1,1) == '['	; to memory
			%error instruction not implemented, MOV %1, %2
		%else
			%error invalid combination of opcode and operands, MOV %1, %2
		%endif
	%else					; to/from normal stuff
		MOV_BASIC_SUBMAC %1,%2
	%endif
%endmacro
; -----------------------------------------------------------------------------
; XCHG operator extension
; extended xchg to permit the following:
; xchg es, ds
; xchg ds, es
; -----------------------------------------------------------------------------
%imacro XCHG 2
	%ifidni %1, cs
		%error invalid instruction XCHG %1, %2
	%elifidni %1, ip
		%error invalid instruction XCHG %1, %2
	%elifidni %2, cs
		%error invalid instruction XCHG %1, %2
	%elifidni %2, ip
		%error invalid instruction XCHG %1, %2
	%elifidni %1, es
		push %1, %2
		pop  %1, %2
	%elifidni %1, ds
		push %1, %2
		pop  %1, %2
	%elifidni %2, es
		push %1, %2
		pop  %1, %2
	%elifidni %2, ds
		push %1, %2
		pop  %1, %2
	%else
		xchg %1, %2
	%endif

%endmacro
; -----------------------------------------------------------------------------
; Conditional JMP extension
; -----------------------------------------------------------------------------
%imacro JAXZ 1-2
	pushf
	test	ax,ax
        jnz	%%Skip
        popf
        jmp	%1 %2
    %%Skip:
    	popf
%endmacro

%imacro JAXNZ 1-2
	pushf
	test	ax,ax
        jz	%%Skip
        popf
        jmp	%1 %2
    %%Skip:
    	popf
%endmacro

; JCXZ exists

%imacro JCXNZ 1-2
        jcxz	%%Skip
        call	%1 %2
    %%Skip:
%endmacro
; -----------------------------------------------------------------------------
; Conditional CALL addition
; -----------------------------------------------------------------------------
%imacro CALLAXZ 1-2
	pushf
	test	ax,ax
        jnz	%%Skip
        popf
        call	%1 %2
        jmp	%%Done
    %%Skip:
    	popf
    %%Done:
%endmacro

%imacro CALLAXNZ 1-2
	pushf
	test	ax,ax
        jz	%%Skip
        popf
        call	%1 %2
        jmp	%%Done
    %%Skip:
    	popf
    %%Done:
%endmacro

%imacro CALLCXZ 1-2
; return if zero flag is set
        jcxnz	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLCXNZ 1-2
; return if zero flag is not set
        jcxz	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLZ 1-2
; return if zero flag is set
        jnz	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLNZ 1-2
; return if zero flag is not set
        jz	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLE 1-2
; return if equal
        jne	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLNE 1-2
; return if not equal
        je	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLC 1-2
; return if carry flag is set
        jnc	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNC 1-2
; return if carry flag is not set
        jc	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLO 1-2
; return if carry flag is set
        jno	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNO 1-2
; return if carry flag is not set
        jo	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLS 1-2
; return if carry flag is set
        jns	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNS 1-2
; return if carry flag is not set
        js	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLP 1-2
; return if carry flag is set
        jnp	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNP 1-2
; return if carry flag is not set
        jp	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLPO 1-2
; return if carry flag is set
        jpe	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLPE 1-2
; return if carry flag is not set
        jpo	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLA 1-2
; return if carry flag is set
        jbe	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLAE 1-2
; return if carry flag is not set
        jb	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLNA 1-2
; return if carry flag is set
        ja	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNAE 1-2
; return if carry flag is not set
        jae	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLB 1-2
; return if carry flag is set
        jnb	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLBE 1-2
; return if carry flag is not set
        ja	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLNB 1-2
; return if carry flag is set
        jb	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNBE 1-2
; return if carry flag is not set
        jbe	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLG 1-2
; return if carry flag is set
        jle	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLGE 1-2
; return if carry flag is not set
        jl	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLNG 1-2
; return if carry flag is set
        jg	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNGE 1-2
; return if carry flag is not set
        jge	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLL 1-2
; return if carry flag is set
        jnl	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLLE 1-2
; return if carry flag is not set
        jg	%%Skip
        call	%1 %2
    %%Skip:
%endmacro

%imacro CALLNL 1-2
; return if carry flag is set
        jl	%%Skip
        call    %1 %2
    %%Skip:
%endmacro

%imacro CALLNLE 1-2
; return if carry flag is not set
        jle	%%Skip
        call	%1 %2
    %%Skip:
%endmacro
; -----------------------------------------------------------------------------
; Conditional LOOP extension
; -----------------------------------------------------------------------------
%imacro LOOPAXZ 1
	pushf
	test	ax,ax
        jnz	%%Skip
        popf
        loop	%1
        jmp	%%Done
    %%Skip:
    	popf
    %%Done:
%endmacro

%imacro LOOPAXNZ 1
	pushf
	test	ax,ax
        jz	%%Skip
        popf
        loop	%1
        jmp	%%Done
    %%Skip:
    	popf
    %%Done:
%endmacro

; LOOPCXZ makes no sense, it would never jump back to %1
; LOOPCXNZ makes no sense, it is what loop does already
; LOOPZ exists
; LOOPNZ exists
; LOOPE exits
; LOOPNE exists

%imacro LOOPC 1
; return if carry flag is set
        jnc	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNC 1
; return if carry flag is not set
        jc	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPO 1
; return if carry flag is set
        jno	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNO 1
; return if carry flag is not set
        jo	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPS 1
; return if carry flag is set
        jns	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNS 1
; return if carry flag is not set
        js	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPP 1
; return if carry flag is set
        jnp	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNP 1
; return if carry flag is not set
        jp	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPPO 1
; return if carry flag is set
        jpe	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPPE 1
; return if carry flag is not set
        jpo	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPA 1
; return if carry flag is set
        jbe	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPAE 1
; return if carry flag is not set
        jb	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPNA 1
; return if carry flag is set
        ja	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNAE 1
; return if carry flag is not set
        jae	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPB 1
; return if carry flag is set
        jnb	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPBE 1
; return if carry flag is not set
        ja	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPNB 1
; return if carry flag is set
        jb	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNBE 1
; return if carry flag is not set
        jbe	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPG 1
; return if carry flag is set
        jle	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPGE 1
; return if carry flag is not set
        jl	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPNG 1
; return if carry flag is set
        jg	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNGE 1
; return if carry flag is not set
        jge	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPL 1
; return if carry flag is set
        jnl	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPLE 1
; return if carry flag is not set
        jg	%%Skip
        loop	%1
    %%Skip:
%endmacro

%imacro LOOPNL 1
; return if carry flag is set
        jl	%%Skip
        loop    %1
    %%Skip:
%endmacro

%imacro LOOPNLE 1
; return if carry flag is not set
        jle	%%Skip
        loop	%1
    %%Skip:
%endmacro
; -----------------------------------------------------------------------------
; Conditional returns
; -----------------------------------------------------------------------------
%imacro RETAXZ 0-1
	pushf
	test	ax,ax
        jnz	%%Skip
        popf
        ret	%1
    %%Skip:
    	popf
    %%Done:
%endmacro

%imacro RETAXNZ 0-1
	pushf
	test	ax,ax
        jz	%%Skip
        popf
        ret	%1
    %%Skip:
    	popf
    %%Done:
%endmacro

%imacro RETCXZ 0-1
; return if zero flag is set
        jcxnz	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETCXNZ 0-1
; return if zero flag is not set1-2
        jcxz	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETZ 0-1
; return if zero flag is set
        jnz	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETNZ 0-1
; return if zero flag is not set
        jz	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETE 0-1
; return if equal
        jne	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETNE 0-1
; return if not equal
        je	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETC 0-1
; return if carry flag is set
        jnc	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNC 0-1
; return if carry flag is not set
        jc	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETO 0-1
; return if carry flag is set
        jno	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNO 0-1
; return if carry flag is not set
        jo	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETS 0-1
; return if carry flag is set
        jns	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNS 0-1
; return if carry flag is not set
        js	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETP 0-1
; return if carry flag is set
        jnp	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNP 0-1
; return if carry flag is not set
        jp	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETPO 0-1
; return if carry flag is set
        jpe	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETPE 0-1
; return if carry flag is not set
        jpo	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETA 0-1
; return if carry flag is set
        jbe	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETAE 0-1
; return if carry flag is not set
        jb	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETNA 0-1
; return if carry flag is set
        ja	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNAE 0-1
; return if carry flag is not set
        jae	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETB 0-1
; return if carry flag is set
        jnb	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETBE 0-1
; return if carry flag is not set
        ja	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETNB 0-1
; return if carry flag is set
        jb	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNBE 0-1
; return if carry flag is not set
        jbe	%%Skip
        ret	%1
    %%Skip:
%endmacro


%imacro RETG 0-1
; return if carry flag is set
        jle	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETGE 0-1
; return if carry flag is not set
        jl	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETNG 0-1
; return if carry flag is set
        jg	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNGE 0-1
; return if carry flag is not set
        jge	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETL 0-1
; return if carry flag is set
        jnl	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETLE 0-1
; return if carry flag is not set
        jg	%%Skip
        ret	%1
    %%Skip:
%endmacro

%imacro RETNL 0-1
; return if carry flag is set
        jl	%%Skip
        ret    %1
    %%Skip:
%endmacro

%imacro RETNLE 0-1
; return if carry flag is not set
        jle	%%Skip
        ret	%1
    %%Skip:
%endmacro

; -----------------------------------------------------------------------------
; not including RETF at present. those should be very rare in COM files.
; -----------------------------------------------------------------------------

; -----------------------------------------------------------------------------
; SDS/SES addition, counterparts to LDS/LES
; -----------------------------------------------------------------------------
%imacro SDS 2
	%if %substr(%str(%1),1,1) == '['
		reg_type %tok(%substr(%str(%1),2,-2))
		%ifdef REG_PAIR
			mov	[REG_PAIR], %2
			mov	[REG_PAIR + 2], ds
		%elifdef REG_16
			mov	[REG_16], %2
			mov	[REG_16 + 2], ds
		%elifdef REG_TOKEN
			mov	[REG_TOKEN], %2
			mov	[REG_TOKEN + 2], ds
		%else
			%warning Parameter %tok(%substr(%str(%1),2,-2)) is REG_CLASS
			%error invalid combination of opcode and operands
		%endif
	%else
		%error invalid combination of opcode and operands
	%endif
%endmacro

%imacro SES 2
	%if %substr(%str(%1),1,1) == '['
		reg_type %tok(%substr(%str(%1),2,-2))
		%ifdef REG_PAIR
			mov	[REG_PAIR], %2
			mov	[REG_PAIR + 2], es
		%elifdef REG_16
			mov	[REG_16], %2
			mov	[REG_16 + 2], es
		%elifdef REG_TOKEN
			mov	[REG_TOKEN], %2
			mov	[REG_TOKEN + 2], es
		%else
			%warning Parameter %tok(%substr(%str(%1),2,-2)) is REG_CLASS
			%error invalid combination of opcode and operands
		%endif
	%else
		%error invalid combination of opcode and operands
	%endif

%endmacro
; -----------------------------------------------------------------------------
; XLAT Extension
; -----------------------------------------------------------------------------
;%imacro XLAT 0-1
;%endmacro
; -----------------------------------------------------------------------------
; Experimental CASE, IFCASE, ELCASE and ENDCASE prototype extension. At present,
; they cannot be nested or use macros that contain them. Eventually, I may
; improve it to support such things. Maybe not. It isn't much more difficult
; to implement CASE like behaviour in assembly. Plus, it can be optimized
; in assembly to minimize condition testing. For example, if you ax can be 0, 1
; or 2. You can cmp ax,2, then ja, jb or fall thru. Eliminating 2 compares and
; a jmp. On the other hand, if you need to check ax agains 0x0d, 0x0a and 0x26
; this would generate similar code. So, IDK.
; -----------------------------------------------------------------------------
%assign SWITCH_CASE_ITEM 0
%imacro case 1
	%ifctx switch
		%error CASE/ENDCASE cannot be nested at present.
	%endif
	%push switch
	%xdefine SWITCH_CASE_REG %1
%endmacro

%imacro ifcase 1
	%ifctx switch
		%ifdef SWITCH_CASE_DONE
			jmp	SWITCH_CASE_DONE
		%else
			%xdefine SWITCH_CASE_DONE SWITCH_CASE_DONE_ %+ SWITCH_CASE_ITEM
		%endif
		SWITCH_CASE_INDEX_ %+ SWITCH_CASE_ITEM:
		%assign SWITCH_CASE_ITEM SWITCH_CASE_ITEM + 1
		cmp	SWITCH_CASE_REG, %1
		jne	SWITCH_CASE_INDEX_ %+ SWITCH_CASE_ITEM

	%else
		%error CASE only valid inside CASE/ENDCASE
	%endif
%endmacro

%imacro elcase 0
	%ifctx switch
		%ifdef SWITCH_CASE_DONE
			jmp	SWITCH_CASE_DONE
		%else
			%xdefine SWITCH_CASE_DONE SWITCH_CASE_DONE_ %+ SWITCH_CASE_ITEM
		%endif
		SWITCH_CASE_INDEX_ %+ SWITCH_CASE_ITEM:
		%assign SWITCH_CASE_ITEM SWITCH_CASE_ITEM + 1
	%else
		%error CASE only valid inside CASE/ENDCASE
	%endif
%endmacro

%imacro endcase 0
	SWITCH_CASE_DONE:
;	%assign SWITCH_CASE_ITEM SWITCH_CASE_ITEM + 1
	SWITCH_CASE_INDEX_ %+ SWITCH_CASE_ITEM:
	%pop
	%undef SWITCH_CASE_DONE
%endmacro
; *****************************************************************************
; %else				; if file has already been included
; *****************************************************************************
%endif
