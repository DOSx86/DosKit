# ver3-master

## DosKit assembly library for DOS development under NASM

BSD 3-Clause License
Copyright (c) 2022-2023, Jerome Shidel
All rights reserved.

## Version

prerelease

## Requirements

[FreeDOS](http://freedos.org/), [DOSBox](https://www.dosbox.com/) or
other DOS based operating system.

## Minimum Build Requirements

[NASM](https://nasm.us/) 2.15.05 or later required
[UPX](https://upx.github.io/) 3.96 or later recommended

[DosKit LIBS](https://gitlab.com/DOSx86/DosKit/-/tree/master/DEVEL/DOSKIT/LIBS)

The original development of DosKit was started on GitHub under
[FDIMPLES](https://github.com/shidel/FDIMPLES/) as the
[LIBS](https://github.com/shidel/FDIMPLES/commit/d30542065066e596db5235fd640fcfeec99d3611).
Now moved to is own [DosKit](https://gitlab.com/DOSx86/DosKit) project for all future development.

# Contributing

Thank you for you interest to improve this project.

### Contributing Language Translations

Please do not submit langauges translations directly to this project. The 
language files *(if any)* that may be in this source tree are **NOT** included 
in the software releases. If any exist in this project, they are for development
purposes only. When a version of this project is built and released, all 
language files are pulled directly from the versions in the 
[FD-NLS](https://github.com/shidel/fd-nls) project. If you wish to provide an 
additional language translation or make corrections to an existing one, please 
update the corresponding files in the [FD-NLS](https://github.com/shidel/fd-nls) 
project.
## DOSKIT.LSM

<table>
<tr><td>title</td><td>DosKit</td></tr>
<tr><td>version</td><td>3.0-pre</td></tr>
<tr><td>entered&nbsp;date</td><td>n/a</td></tr>
<tr><td>description</td><td>Library for DOS development with NASM</td></tr>
<tr><td>summary</td><td>Assembly library for DOS 8086+ application development. Requires NASM 2.15.05 or newer.</td></tr>
<tr><td>keywords</td><td>dos 16 bit, asm</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://gitlab.com/DOSx86/DosKit</td></tr>
<tr><td>original&nbsp;site</td><td>https://github.com/shidel/FDIMPLES/tree/devel</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[BSD 3-Clause License](LICENSE)</td></tr>
</table>
