## DosKit assembly library for DOS development under NASM

BSD 3-Clause License
Copyright (c) 2022-2023, Jerome Shidel
All rights reserved.

## Version

prerelease

## Requirements

[FreeDOS](http://freedos.org/), [DOSBox](https://www.dosbox.com/) or
other DOS based operating system.

## Minimum Build Requirements

[NASM](https://nasm.us/) 2.15.05 or later required
[UPX](https://upx.github.io/) 3.96 or later recommended

[DosKit LIBS](https://gitlab.com/DOSx86/DosKit/-/tree/master/DEVEL/DOSKIT/LIBS)

The original development of DosKit was started on GitHub under
[FDIMPLES](https://github.com/shidel/FDIMPLES/) as the
[LIBS](https://github.com/shidel/FDIMPLES/commit/d30542065066e596db5235fd640fcfeec99d3611).
Now moved to is own [DosKit](https://gitlab.com/DOSx86/DosKit) project for all future development.
